/* 	Appunti del Corso di Programmazione II (M-Z) - Docente Simone Faro
	Università degli Studi di Catania - Dipartimento di Matematica e Informatica
	Corso di Laurea Triennale in Informatica
*/


#include "Coppia.h"
#include <iostream>
using namespace std;


int main() {
	Coppia c;
	c.print();

	Coppia a = c;
	a.print();

	Coppia b(5,6);
	b.print();		
	
	Coppia d = Coppia(5,6);
	d.print();	

	Coppia e = Coppia(d);
	e∏.print();	
}
