/* 	Appunti del Corso di Programmazione II (M-Z) - Docente Simone Faro
	Università degli Studi di Catania - Dipartimento di Matematica e Informatica
	Corso di Laurea Triennale in Informatica
*/

#include <iostream>
using namespace std;

// Questa classe rappresenta una coppia di elementi

class Coppia {
	private:
		int x,y;
		
	public:
		Coppia();
		Coppia(int x, int y);
		Coppia(const Coppia& c);
		void setX(int x);
		void setY(int y);
		void print();
};

Coppia::Coppia() {
	x = 0;
	y = 0;
}

Coppia::Coppia(int x, int y) {
	this->x = x;
	this->y = y;
}

Coppia::Coppia(const Coppia& c) {
	this->x = c.x+1;
	this->y = c.y+1;
}

void Coppia::setX(int x) {
	this->x = x;
}

void Coppia::setY(int y) {
	this->y = y;
}

void Coppia::print() {
	cout << "\n(" << x << ", " << y << ")\n";
}
