/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle classi derivate
Esercizio 4: Esempio di uso di distruttori in classi derivate. 
*/
#include<iostream>
using namespace std; 

class C1 {
public: 
	C1(int n);
	~C1();
private:  
	int *pi,l; 
};

C1::C1(int n): l(n) {
// Assegnamo ad l il valore di n
	cout << "Assegno " << l << " nuovi interi. " << endl; 
	pi= new int[l]; 
}

C1::~C1() {
	cout << "Elimino " << l << " interi. " << endl; 
	delete[] pi; 
}

// C2 classe derivata di C1 
class C2 : public C1{
public: 
	C2(int n);
	~C2();
private:  
	char *pc; 
	int l; 
};

C2::C2(int n):C1(n),l(n) {
/* Qui l(n) è necessario altrimenti l non verrebbe inizializzato.
	La virgola separa i due inizializzatori
*/
	cout << "Assegno " << l << " nuovi caratteri. " << endl; 
	pc= new char[l]; 
}

C2::~C2() {
	cout << "Elimino " << l << " caratteri. " << endl; 
	delete[] pc; 
}
	

int main()
{
	C2 a(50), b(100);
	return 0;
}
		
