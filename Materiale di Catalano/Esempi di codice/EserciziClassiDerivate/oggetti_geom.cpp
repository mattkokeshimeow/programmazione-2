/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle classi derivate

Esercizio 1: Definizione di una classe Oggetto geometrico e delle classi 
derivate cerchio e quadrato. E' un esempio di eredità pubblica.
*/
#include<iostream>
using namespace std; 
const float PI=3.14159265;


class Oggetto_Geometrico {
public: 
	Oggetto_Geometrico(float x=0, float y=0) : xC(x), yC(y) {} 
	/*
	Vettore inizializzazione di membri: subito dopo la 
	lista dei parametri nella definizione del costruttore. 
	Consiste nel carattere : seguito da uno o più inizializzatori 
	di membro separati da virgole. 
	Serve per assegnare costanti e riferimenti
	*/
	void stampa_centro ()
	{
		cout << xC << " " << yC << endl; 
	}
protected: 
	float xC,yC;
};

class cerchio : public Oggetto_Geometrico {
// Cerchio eredita tutte le componenti pubbliche di Oggetto_Geometrico
public: 
	//Costruttore
	cerchio(float x_C, float y_C, float r) : 
		Oggetto_Geometrico(x_C, y_C) 
		{
			raggio=r;
		}
	float area() const {return PI*raggio*raggio;}
	// Funzione costante: non può cambiare gli attributi 
	// dei suoi oggetti
private:
	float raggio; 
}; 

class quadrato: public Oggetto_Geometrico {
public: 
	quadrato(float x_C, float y_C, float x, float y) :
/* Un quadrato si può determinare dal suo centro e uno dei quattro 
vertici*/	
 		Oggetto_Geometrico(x_C,y_C)
 		{
 			xVertice=x;
 			yVertice=y; 
 		}
	float area() const 
	{	
		float a,b;
		a=xVertice-xC;
		b=yVertice-yC; 
		return 2*(a*a+b*b); 
	/* (a*a)+(b*b) è la metà del quadrato della diagonale */
	}
private: 
	float xVertice, yVertice; 
};

int main()
{
	cerchio C(2,2.5,2);
	quadrato Q(3,3.5,4.37,3.85);
	
	cout << "Centro del cerchio: ";
	C.stampa_centro(); 
	cout << "Centro del quadrato: "; 
	Q.stampa_centro(); 
	cout << "Area del cerchio: " << C.area() << endl;
	cout << "Area del quadrato: " << Q.area() << endl;
	return 0;
}
		
