/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle classi derivate

Esercizio 7: Definizione di una classe figura come classe base e delle classi 
derivate cerchio e triangolo. E' un esempio di uso di funzioni virtuali.
*/
#include<iostream>
using namespace std; 
#define PI 3.1415

class Figura {
public: 
	Figura() {}
	virtual double area() {};
	// Senza le parentesi graffe non funziona! 
private:
	int ciao;
}; 

class cerchio : public Figura {
public: 
	cerchio(double x): raggio(x) {}
	// Costruttore
	double area(); 
private: 
	double raggio; 
};

double cerchio::area() {
	return (PI*raggio*raggio); 
}

class quadrato : public Figura {
// Cerchio eredita tutte le componenti pubbliche di figura
public: 
	quadrato(int x) : Figura(),lato(x) {}
	//Costruttore
	double area(void) const;
private: 
	double lato; 
}; 

double quadrato::area(void) const
{
	return (lato*lato);
}


int main()
{
	cerchio C(2);
	quadrato Q(6);
	
	
	cout << "Area del cerchio: " << C.area() << endl;
	cout << "Area del quadrato: " << Q.area() << endl;
	return 0;
}
		