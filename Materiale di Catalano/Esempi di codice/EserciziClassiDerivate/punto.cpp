/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle classi derivate

Esercizio 3: Definizione di una classe Punto e della classe 
derivata Punto3D. Esempio di uso di costruttori in classi derivate. 
*/
#include<iostream>
using namespace std; 

class Punto {
/* 	La classe punto contiene il costruttore e una funzione per scrivere 
	le coordinate del punto */
public: 
	Punto(int xx, int yy);
	void ScriviPunto();
private:  
	int x,y; 
};

class Punto3D : public Punto {
/*	La classe Punto3D è derivata dalla classe Punto. 
	Contiene una funzione per inizializzare z, una funzione di stampa e il 
	costruttore  */	
public: 
	Punto3D(int xx, int yy, int zz); 
	void FissaZ(int zz);
	void Stampa3D();
private: 
	int z; 
};

// Il costruttore di Punto è definito fuori dalla dichiarazione della classe
Punto:: Punto(int xx, int yy) {
	x=xx; 
	y=yy; 
	}

void Punto::ScriviPunto()
{	cout << "Valore di x: " << x << endl; 
 	cout << "Valore di y: " << y << endl;
}


//Sintassi di un costruttore di classe derivata 
//Il costruttore appare fuori dal prototipo della funzione
Punto3D::Punto3D(int xx, int yy, int zz): Punto (xx,yy){ 
FissaZ(zz);
}

void Punto3D::Stampa3D()
{
	ScriviPunto();
	cout << "Valore di z: " << z << endl;
} 
	

void Punto3D::FissaZ(int zz)
{
	z=zz;
} 
	

int main()
{
	Punto p(2,6);
	Punto3D pp(4,5,8);
	
	p.ScriviPunto(); 
	pp.Stampa3D();
	return 0;
}
		
