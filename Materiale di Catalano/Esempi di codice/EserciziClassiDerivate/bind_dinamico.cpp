/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle classi derivate

Esercizio 6: Definizione di due classe A e B per illustrare l'uso di binding dinamico.
*/
#include<iostream>
using namespace std; 

class ClasseA {
public: 
	ClasseA() {}
	//Costruttore
	virtual void Dinamica() {
		cout << "Funzione dinamica della Classe A" << endl; 
		}
	void Statica() {
		cout << "Funzione statica della Classe A" << endl; 
		}  
}; 

class ClasseB : public ClasseA {
public: 
	ClasseB() {}
	// Costruttore
	void Dinamica() {
	//Qui virtual non serve in quanto tale dichiarazione va fatta sonlo nella prima classe 
	//in cui la funzione è presente (tipicamente quella di livello più alto in gerarchia)
		cout << "Funzione dinamica della Classe B" << endl; 
		}
	void Statica() {
		cout << "Funzione statica della Classe B" << endl; 
		}  
};

int main()
{
	ClasseA* a;
	ClasseB* b;  
	
	a=new ClasseA();
	b=new ClasseB();
	
	cout << "Funzioni dell'oggetto a della classe A" << endl;
	a->Dinamica();
	a->Statica(); 
	cout << endl; 
	cout << "Funzioni dell'oggetto a della classe B" << endl;
	b->Dinamica();
	b->Statica(); 
	cout << endl;
	a=b; 
	//Proprietà del polimorfismo: un puntatore ad una classe derivata è anche un
	//puntatore alla classe base 
	cout << "Funzioni dell'oggetto a della classe A al" 
		<<	" quale abbiamo assegnato il valore di b" << endl;
	a->Dinamica();
	a->Statica(); 
	return(0);
}

