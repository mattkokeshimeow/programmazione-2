/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle classi

Esercizio 5: Definizione di una classe voti. Versione su più file.
*/
#include<iostream>
#include"Voti.h"

using namespace std;

void Voti::inserisci(double voto) 
{
	voti[n++]=voto; 
}
 
Voti::Voti() {
	n=0;
	for(int i=0; i<MAX;i++)
		voti[i]=0;
	}	

double Voti::media_voti() {
	double somma=0; 
	for(int i=0;i<n;i++) 
		somma+=voti[i];
	return (somma/n);
	}

