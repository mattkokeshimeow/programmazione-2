/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle classi

Esercizio 5: Definizione di una classe voti. Versione su più file. 
*/
// Qui mettere il define è opzionale perché MAX può essere anche definito nel file principale.
#define MAX 100


class Voti	{
		double voti[MAX]; // campo privato
		int n; 
	public: 
		Voti(); // Costruttore
		void inserisci(double voto); 
		double media_voti(); 
		//double voto_massimo();
		//double voto_minimo();
		};

