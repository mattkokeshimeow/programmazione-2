/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle classi

Esercizio 5: Definizione di una classe voti. Versione su più file.
*/
#include<iostream>
#include"Voti.h"
using namespace std;
//#define MAX 100

int main()
{
	Voti voti; 
	bool flag=1;
	//voti.Voti(); 
	// Il costruttore non deve essere 
	// invocato esplicitamente
	cout << "Inserisci i voti (e un numero negativo per terminare)\n"; 
	do {
		// bool flag=1;
		// Dichiarare flag qua darebbe errore perché 
		// la variabile sarebb invisibile al while
		double voto; 
		cout << "-- ";
		cin >> voto;
		if (voto<0) flag=0;
		else voti.inserisci(voto); 
	}	while(flag);
	double media=voti.media_voti();
	cout << "La media è: " << media << endl; 
	
} 