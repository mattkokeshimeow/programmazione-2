/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle classi

Esercizio 2: Definizione di una classe rettangolo.
In cui la funzione modifica è dichiarata all'interno della classe 
e definita fuori.
*/
#include<iostream>
using namespace std;

class rettangolo {
	public: 
		double area(){ // definizione 
			return base*altezza; 
			}
		void modifica(double , double); 
		//dichiarazione della funzione modifica			
	private: 
		double base;
		double altezza;
}; 

void rettangolo::modifica(double Alt, double Base) {
			base=Base; 
			altezza=Alt; 
			}
    
int main()
{
	rettangolo r; 
	double area;

	r.modifica(5,7); 
	area=r.area(); 
	cout << "L'area è " << area << endl;
}