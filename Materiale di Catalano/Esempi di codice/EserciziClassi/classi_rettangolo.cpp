/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle classi

Esercizio 1: Definizione di una classe rettangolo. 
La definizione di una classe non riserva spazio in memoria.
*/
#include<iostream>
using namespace std;

class rettangolo {
	public: 
		double area(){ // definizione 
			return base*altezza; 
			}
		void modifica(double Alt, double Base) {
			base=Base; 
			altezza=Alt; 
			}
	private: 
		double base;
		double altezza;
}; 

    
int main()
{
	rettangolo r; 
	double area;

	r.modifica(5,7); 
	area=r.area(); 
	cout << "L'area è " << area << endl;
}