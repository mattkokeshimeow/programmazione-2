/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sui templates

Esercizio 4: Esempio di compilazione per inclusione.
*/
#ifndef DEMO
#define DEMO
template <class T> int confrontare (T, T);
#include "demo.cpp"
#endif
