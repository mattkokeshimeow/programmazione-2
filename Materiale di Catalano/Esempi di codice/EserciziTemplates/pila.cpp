/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sui templates

Esercizio 3: Definizione di una classe Pila usando templates di classe.
*/
#include<iostream>
using namespace std;

enum stato_pila {OK, PIENA, VUOTA};

template <class T> class Pila  {
public: 
	Pila (int l=10); //Costruttore
	~Pila() {delete [] tabella;} //Distruttore
	void immettere(T); 
	T estrarre();
	void visualizza();
	int num_elementi();
	int leggi_lunghezza() {return lunghezza;}
private: 
	int lunghezza; 
	T* tabella; 
	int cima; 
	stato_pila stato; 
};

/* Pila è una classe parametrizzata per un tipo T. 
Questo si rappresenta con la notazione Pila <T> */

template <class T> Pila <T> :: Pila (int lung) 
{
	lunghezza=lung;
	tabella= new T[lunghezza]; 
	cima=-1;
	stato=VUOTA; 
}

template <class T> void Pila <T> :: immettere (T elem) 
{
	if (stato!=PIENA) 
		tabella[++cima]=elem;
	else
		cout << "Pila piena! " << endl;
	if (cima== lunghezza-1)
		stato=PIENA; 
	else stato=OK;
}  

template <class T> T Pila <T> :: estrarre () 
{
	T elem=0;
	if (stato!=VUOTA) 
		elem=tabella[cima--];
	else
		cout << "Pila vuota! " << endl;
	if (cima== -1)
		stato=VUOTA; 
	else stato=OK;
	return elem;
}  

template <class T> void Pila <T>::visualizza()
{
	for(int i=cima; i>=0; i--)
		cout << "[" << tabella[i] << "]" << endl; 
}
    
template <class T> int Pila <T>:: num_elementi()
{
	return cima+1; 
}    
    
int main()
{
	Pila <int> stack(6); //Pila di interi
	Pila <char> stack2; //Pila di caratteri
	 
	stack.immettere(1);
	stack.immettere(2);
	stack.immettere(3); 
	cout << "Numero di elementi: " << stack.num_elementi() 
		 << endl; 
	stack.visualizza();
	cout << "Estrarre il primo: " << stack.estrarre() << endl; 
	cout << "Estrarre il secondo: " << stack.estrarre() << endl; 
	cout << "Estrarre il terzo: " << stack.estrarre() << endl;  
	cout << "Numero di elementi: " << stack.num_elementi() 
		 << endl; 
	cout << "Estrarre il quarto: " << stack.estrarre() << endl; 
	cout << endl;
	
	stack2.immettere('A');
	stack2.immettere('B');
	stack2.immettere('C');
	stack2.immettere('D'); 
	cout << "Numero di elementi: " << stack2.num_elementi() 
		 << endl; 
	stack2.visualizza();
	cout << "Estrarre il primo: " << stack2.estrarre() << endl; 
	cout << "Estrarre il secondo: " << stack2.estrarre() << endl; 
	cout << "Estrarre il terzo: " << stack2.estrarre() << endl; 
	cout << "Estrarre il quarto: " << stack2.estrarre() << endl;  
	cout << "Numero di elementi: " << stack2.num_elementi() 
		 << endl; 
	cout << endl;
	return 0;
}