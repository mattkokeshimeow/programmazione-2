/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sui template

Esercizio 2: Definizione di un template per la funzione ricerca 1 elemento generico 
in un vettore di elementi dello stesso tipo.
*/

#include<iostream>
using namespace std; 
const int DIM=12;
const int DIMf=13;

template <class T> int ricerca(T dato, T vettore[], int dimensione) 
{	int flag=-1, i=0;

	while ((flag==-1) && (i<dimensione))  
	{
		 if (dato==vettore[i]) flag=i;
		 //cout << "Ciao " << i << endl; 
		 i++;
	}
	return flag; 
} 

int main()
{
	int d1, ivett[DIM]={4,5,23,2,12,32,7,67,53,98,51,3};
	float f1,fvett[DIMf]={5.3,3.0,9,8,7.1,9.9,11.1,6.42,78.1,1.234,77,21,5.1};
	int ris; 
	
	cout << "Inserire un intero: " ;
	cin >> d1;
	ris=ricerca(d1,ivett,DIM); 
	if (ris!=-1) 
		cout << "L'elemento cercato si trova in posizione " << ris <<endl;
	else 
		cout << "Elemento non trovato" << endl; 
	cout << "Inserire un razionale: " ;
	cin >> f1;
	ris=ricerca(f1,fvett,DIMf); 
	if (ris!=-1) 
		cout << "L'elemento cercato si trova in posizione " << ris <<endl;
	else 
		cout << "Elemento non trovato" << endl; 
	return (0);  
}