/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sui template

Esercizio 1: Definizione di un template per la funzione che scambia due elementi.
*/

#include<iostream>
using namespace std; 

template <class T> void scambia(T& a, T& b) 
{
	T aux; 
	
	aux=a; 
	a=b; 
	b=aux;
} 

int main()
{
	int i1=5,i2=7;
	float f1=3.2,f2=4.3;
	char c1='A',c2='B';
	
	cout << "Valori originali: " << i1 << " " << i2 <<endl; 
	scambia(i1,i2);
	cout << "Valori scambiati: " << i1 << " " << i2<<endl; 
	cout << "Valori originali: " << f1 << " " << f2<<endl; 
	scambia(f1,f2);
	cout << "Valori scambiati: " << f1 << " " << f2<<endl;
	cout << "Valori originali: " << c1 << " " << c2<<endl; 
	scambia(c1,c2);
	cout << "Valori scambiati: " << c1 << " " << c2<<endl;
	return (0);  
}