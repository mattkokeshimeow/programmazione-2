/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sui templates

Esercizio 4: Esempio di compilazione per inclusione.
*/
#include<iostream>
#include"demo.h"
using namespace std;

    
int main()
{
	int a,b,flag; 
	float c,d; 
	
	cout << "Inserire due interi: "; 
	cin >> a >> b; 
	flag=confrontare(a,b);
	if (flag==1) cout << "Il più grande è il primo" << endl; 
	else cout << "Il più grande non è il primo" << endl;
	cout << "Inserire due razionali: "; 
	cin >> c >> d; 
	flag=confrontare(c,d);
	if (flag==1) cout << "Il più grande è il primo" << endl; 
	else cout << "Il più grande non è il primo" << endl;
	return 0;
}