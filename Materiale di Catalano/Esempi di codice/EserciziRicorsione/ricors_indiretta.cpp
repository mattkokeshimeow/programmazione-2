/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulla ricorsione
Esercizio 2: Esempio di ricorsione indiretta. Scrivere (in sequenza) i numeri 5 a 25.
*/
#include<iostream>
using namespace std;

void A (int); 
void B (int); 

void A(int c) 
{	if (c> 5) B(c); 
	cout << c << " ";}

void B(int c)
{	A(--c);  }

int main() 
{
	A(25);
}
