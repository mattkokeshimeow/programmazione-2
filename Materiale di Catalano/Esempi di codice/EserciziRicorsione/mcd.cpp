/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulla ricorsione
Esercizio 3: Calcolare il MCD di due interi (versione iterativa e versione ricorsiva).
*/
#include<iostream>
using namespace std;

int MCD(int m, int n) 
{
 	if ((n<=m) && (m%n==0)) return n;
 	else if (m<n) return MCD(n,m);
 		else return MCD(n,m%n);
} 

int Calcola_MCD(int m, int n)
{
	int a=m, b=n,aux; 
	
	while (b!=0) {
		aux=b;
		b=a%aux; 
		a=aux;
		}
	return a;
} 

int MCD_iterativo(int m, int n)
{
	if (m>n) Calcola_MCD(m,n);
	else Calcola_MCD(n,m);
} 


int main() 
{	int m,n;
	
	cout << "Inserire due interi positivi: ";
	cin >> m;
	cin >> n;  
	if ((n<0) || (m<0)) cout << "Errore: numeri non positivi \n"; 
	else {
		cout << "Il massimo comune divisore è :" << MCD(m,n) << endl; 
		cout << "Lo stesso risultato calcolato iterativamente : " 
			 << MCD_iterativo(m,n) << endl;
		} 
}
