/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sui puntatori

Esercizio 5: Bubble sort utilizzando i puntatori.

*/
#include <iostream>
using namespace std;

// procedura di stampa che deferenzia le posizioni dell'array
void stampa(int* v[], int n) {
	for(int i=0; i<n; i++)
		cout << **(v+i) << " \t";
	cout << endl;
}

// procedura di stampa
void stampaInt(int v[], int n) {
	for(int i=0; i<n; i++)
		cout << *(v+i) << " \t";
	cout << endl;
}

void bubblesortPointer(int* v[], int n) {
	int i,k;
	int temp;
	for(i = 0; i<n-1; i++)
		for(k = 0; k<n-1-i; k++)
			if(*v[k] > *v[k+1]) {
				int *temp = v[k];
				v[k] = v[k+1];
				v[k+1] = temp;
			}
}

int main() {
	int  A[] = {2,6,5,71,14,24,243,21,15,31};
	int* B[10]; // deve essere specificata la dimensione di B
	
	for(int i=0; i<10; i++) B[i] = A+i; // sono entrambi puntatori a int
	cout << "A: "; stampaInt(A, 10);
	cout << "B: "; stampa(B, 10);
	bubblesortPointer(B, 10);
	cout << "A: "; stampaInt(A, 10);
	cout << "B: "; stampa(B, 10);
	return 0; 
}

