/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sui puntatori

Esercizio 2: Puntatori e array
*/
#include <iostream>
using namespace std;

// procedura di stampa classica che utilizza gli indici
void stampa1(int v[], int n) {
	for(int i=0; i<n; i++)
		cout << v[i] << "\t";
	cout << endl;
}

// procedura di stampa che deferenzia le posizioni dell'array
void stampa2(int v[], int n) {
	for(int i=0; i<n; i++)
		cout << *(v+i) << " \t";
	cout << endl;
}

// procedura di stampa che utilizza un puntatore locale 
// inizializzato all'array
void stampa3(int v[], int n) {
	int* p = v;
	for(int i=0; i<n; i++) {
		cout << *p << " \t";
		p++; // p ora punta alla successiva locazione di memoria
	}
	cout << endl;
}

int main() {
	int A[] = {3,6,12,87,34,21,73,45,15,29};
	stampa1(A,10);	
	stampa2(A,10);	
	stampa2(A,10);	
	return 0; 
}