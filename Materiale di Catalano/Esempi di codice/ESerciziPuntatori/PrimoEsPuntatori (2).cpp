/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sui puntatori

Esercizio 1: Primo esempio di utilizzo di puntatori.

*/
#include<iostream>
using namespace std;

int main()
{ 
	int n=75;
	int* p= &n; 
	
	cout << "n=" << n << ", &n=" << &n << ", p=" << p << endl;
	cout << "&p=" << &p << endl;
	return 0; 
}
