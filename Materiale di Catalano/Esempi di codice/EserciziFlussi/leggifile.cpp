/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi su Flussi e Files

Esercizio 3: Legge un file test e lo mostra in output a video  
*/
#include<fstream>
#include<iostream>

using namespace std;

int main()
{
	char Nome[30]; 
	
	cout << "Inserire il nome del file. "; 
	cin >> Nome;
	ifstream FileIn(Nome);
	if (!FileIn) 
	{	cerr << "Impossibile aprire il file " << Nome << endl;
		return -1; 
	}
	char c; 
	while(FileIn.get(c))
		cout.put(c); 
	return 0;	
}