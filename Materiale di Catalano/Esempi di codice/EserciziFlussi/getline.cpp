/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi su Flussi e Files

Esercizio 1: Problemi con l'uso di getline. 
La pressione del tasto invio dopo aver inserito l'età fa si 
che il carattere \n rimanga nel buffer di memoria. 
*/
#include<iostream>

using namespace std;

int main()
{
	char Nome[30]; 
	int eta; 	
	
	cout << "Introdurre l'età: "; 
	cin >> eta; 
	cout << eta << endl;
	cout << "Introdurre il nome: ";  
	cin.getline(Nome,30); 
	cout << Nome << endl; 
	return 0;	
}