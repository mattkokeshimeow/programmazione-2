/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi su Flussi e Files

Esercizio 4: Scrittura di un file contenente i dati inseriti dall'utente  
*/
#include<fstream>
#include<iostream>

using namespace std;

int main()
{
	ofstream FileOut("Prova");
	if (!FileOut) 
	{	cerr << "Impossibile aprire il file Prova " << endl;
		return -1; 
	}
	char c; 
	while(cin.get(c))
		FileOut.put(c); 
	//Per uscire dal while bisogna premere ctrl D
	return 0;	
}