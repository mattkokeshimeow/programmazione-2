/* 
Corso di Programmazione II (A-L), Prof. Dario Catalano 

Esercizi sulle funzioni

Esercizio 2: Incrementare un contatore usando variabili locali
(esempio di utilizzo di variabili static)
*/
#include<iostream>
using namespace std;


void EsempioStatic(int Call)
{
  static int contatore;
  
  if (Call==1) contatore=100;
  cout << "\n Alla chiamata " << Call << " il contatore è settato a : " 
  << contatore << endl; 
  ++contatore;   
}


int main()
{
 EsempioStatic(1);
 EsempioStatic(2); 
 EsempioStatic(3);
 return 0; 
}