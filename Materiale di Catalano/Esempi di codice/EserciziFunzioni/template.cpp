#include<iostream>
using namespace std;

template <class T> T miomax(T a, T b)
{ 
	if (a > b) return a;
	else return b;
} 
	
//int miomax(int, int);
//float miomax(float, float); 

int main()
{ 	int a=12; int b=23;
	float e=3.4; float f=5.7; 

 	cout << "Il massimo tra " << a << " e " << b << " è " << miomax(a,b) << endl;
  	cout << "Il massimo tra " << e << " e " << f << " è " << miomax(e,f) << endl;
  	return 0; 
}