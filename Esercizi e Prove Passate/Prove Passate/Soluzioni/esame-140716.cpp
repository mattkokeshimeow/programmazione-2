	

    #include<iostream>
    using namespace std;
     
     
    template <class H > class List{
            public:
                    virtual List <H>* ins(H * x) =0;
                    virtual List<H>* del (H* x) =0;
                    virtual int search (H*x) =0;
                    virtual void print() =0;
            };
    template<class H> class Stack{
            public:
                    virtual void push(H* x)=0;
                    virtual H pop()=0;
            };
    template <class H> class Queue {
            public:
                    virtual void enqueue(H *x) = 0;
                    virtual H dequeue() = 0;
            };
     
           
    template <class H>
            struct nodo{
                    H val;
                    nodo*next;
                    };
                   
    template <class H>
            class MyList  : public List<H>{
                    public:
                            MyList(){this->inizio=NULL;}
                            ~MyList(){}
                            nodo<H>*inizio;
                            MyList <H>* ins(H * x) {
                                    nodo<H>* nuovo=new nodo<H>;
                                    nuovo->val=*x;
                                    nuovo->next=NULL;
                                    if (this->inizio==NULL) {this->inizio=nuovo;}
                                    else{
                                            nodo<H>*app=this->inizio;
                                            nodo<H>*app2;
                                            while(app!=NULL){app2=app;
                                                    app=app->next;}
                                                    app2->next=nuovo;
                                            }
                                            return this;
                            };
                            MyList<H>* del (H* x) {
                                    nodo<H>*app=this->inizio;
                                    nodo<H>*app2;
                                    while (app !=NULL && app->val!=*x){
                                            app2=app;
                                            app=app->next;
                                            }
                                            if (app2->next ==NULL) cout<<"elemento non trovato "<<endl;
                                            else {
                                                    if(app!=this->inizio){
                                                    app2->next=app->next;
                                                    }
                                                    else{
                                                            this->inizio=this->inizio->next;
                                                            }
                                                    }
                                                    delete app;
                                                    return this;
                                    };
                            int search (H*x) {
                                    nodo<H>*app=this->inizio;
                                    while(app!=NULL && app->val!=*x)
                                            app=app->next;
                                    return (app==NULL ?  0:1);
                                    };
                            void print() {
                                    nodo<H>*app=this->inizio;
                                    while(app!=NULL){
                                            cout<<" "<<app->val;
                                            app=app->next;
                                            }
                                            cout<<endl;
                                    };
                    };
                   
                    template <class H>
                    class OrderedList : public MyList<H>{
                            public:
                                    OrderedList<H>*ins(H*x){
                                            nodo<H>*nuovo=new nodo<H>;
                                            nuovo->val=*x;
                                            nuovo->next=NULL;
                                            if(this->inizio==NULL)
                                                    this->inizio=nuovo;
                                            else{
                                                    nodo<H>*app=this->inizio;
                                                    nodo<H>*app2;
                                                    while(app!=NULL && app->val <nuovo->val){
                                                            app2=app;
                                                            app=app->next;
                                                            }
                                                            if(a!=this->head){
                                                                    nuovo->next=b->next;
                                                                    b->next=nuovo;
                                                                    }
                                                                    else{
                                                                             nuovo->next=a;
                                                                            this->inizio=nuovo;
                                                                            }
                                                    }
                                            };
                            };
                   
                   
                    template <class H>
                            class MyStack :public MyList<H>, public Stack<H>{
                                    public:
                                                     void push(H* x){
                                                             nodo<H>*nuovo=new nodo<H>;
                                                             nuovo->val=*x;
                                                             nuovo->next=NULL;
                                                             nuovo->next=this->inizio;
                                                             this->inizio=nuovo;
                                                             };
                                                     H pop(){
                                                             if(this->inizio==NULL ){return -1;}
                                                             else{
                                                                     H x=this->inizio->val;
                                                                     this->inizio=this->inizio->next;
                                                                     return x;
                                                                     }
                                                             };
                                    };
                    template <class H>
                                    class MyQueue:  public MyList <H> ,public Queue<H>{
                                            public:
                                                     void enqueue(H *x) {
                                                             nodo<H>*nuovo=new nodo<H>;
                                                             nuovo->next=NULL;
                                                             nuovo->val=*x;
                                                             if(this->inizio==NULL) this->inizio=nuovo;
                                                             else{
                                                                     nodo<H>*app=this->inizio;
                                                                     while(app->next!=NULL){
                                                                             app=app->next;
                                                                             }
                                                                             app->next=nuovo;
                                                                     }
                                                             };
                                                     H dequeue() {
                                                             if(this->inizio==NULL) {return -1;}
                                                             else{
                                                             nodo<H>*app=this->inizio;
                                                                    H x=app->val;
                                                                    this->inizio=app->next;
                                                                     delete app;
                                                                     return x;
                                                             }
                                                             };
                                            };
                   
                    int main(){
                            int dainserire[]={23,4,6,8,12,21,5,9,7,3,16,2,24};
                            int daeliminare[]={4,5,16,2};
                            MyList<int>*mia=new MyList<int>();
                            OrderedList<int>*omia=new OrderedList<int>();
                            MyStack <int>*smia=new MyStack<int>();
                            MyQueue<int>*qmia=new MyQueue<int>();
                           
                            for (int i =0;i<13;i++)
                                    mia->ins(&dainserire[i]);
                            for(int i=0;i<4;i++)
                                    mia->del(&daeliminare[i]);
                           
                            mia->print();
                            
                          
                            for (int i =0;i<13;i++)
                                    omia->ins(&dainserire[i]);
                            for(int i=0;i<4;i++)
                                    omia->del(&daeliminare[i]);
                           
                            omia->print();
                           
                           
                            for (int i =0;i<13;i++)
                                    smia->push(&dainserire[i]);
                            for(int i=0;i<5;i++)
                                    smia->pop();
                           
                            smia->print();
                           
                            for (int i =0;i<13;i++)
                                    qmia->enqueue(&dainserire[i]);
                           
                            for(int i=0;i<5;i++)
                                    qmia->dequeue();
                                   
                            qmia->print();
                           return 8;
                            }


