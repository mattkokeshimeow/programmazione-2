#include <iostream>

using namespace std;

template <class H>
class LNode
{
	private:
		LNode<H> *prev, *next;
		H *val;
	public:
		LNode(H *val=NULL){this->val=val ? new H(*val) : NULL; prev=next=NULL;}
		void setPrev(LNode<H> *prev){this->prev=prev;}
		void setNext(LNode<H> *next){this->next=next;}
		void setVal(H* val){this->val=val;}
		LNode<H> *getPrev(){return prev;}
		LNode<H> *getNext(){return next;}
		H *getVal(){return val;}
};


template <class H>
class TNode
{
	private:
		TNode<H> *left, *right, *parent;
		H *val;
	public:
		TNode(H *val=NULL){this->val=val ? new H(*val) : NULL; left=right=parent=NULL;}
		void setParent(TNode<H> *parent){this->parent=parent;}
		void setLeft(TNode<H> *left){this->left=left;}
		void setRight(TNode<H> *right){this->right=right;}
		void setVal(H* val){this->val=val;}
		TNode<H> *getLeft(){return left;}
		TNode<H> *getRight(){return right;}
		TNode<H> *getParent(){return parent;}
		H *getVal(){return val;}
};

template <class H>
class DList
{
	private:
		LNode<H> *header, *trailer, *current;
	public:
		DList(){header=new LNode<H>(); trailer=new LNode<H>(); header->setNext(trailer); trailer->setPrev(header);}
		
		DList<H> *ins(H x)
		{
			LNode<H> *tmp = header->getNext();
			while(tmp!=trailer && x>*tmp->getVal())
			{
				tmp=tmp->getNext();
			}
			LNode<H> *nd = new LNode<H>(&x);
			nd->setPrev(tmp->getPrev());
			tmp->getPrev()->setNext(nd);
			nd->setNext(tmp);
			tmp->setPrev(nd);
			return this;
		}
		
		DList<H> *canc(H x)
		{
			LNode<H> *tmp = _search(x);
			if(tmp)
			{
				tmp->getPrev()->setNext(tmp->getNext());
				tmp->getNext()->setPrev(tmp->getPrev());
				delete tmp;
			}
			return this;
		}
		
		LNode<H> *_search(H x)
		{
			LNode<H> *tmp = header->getNext();
			while(tmp!=trailer)
			{
				if(x==*tmp->getVal()) return tmp;
				tmp=tmp->getNext();
			}
			return NULL;
		}
		
		H *search(H x)
		{
			LNode<H> *tmp = _search(x);
			if(tmp) return tmp->getVal();
			return NULL;
		}
		
		void print()
		{
			LNode<H> *tmp = header->getNext();
			while(tmp!=trailer)
			{
				cout<<*tmp->getVal()<<"\t";
				tmp=tmp->getNext();
			}
			cout<<endl;
		}
		
		H *begin(){current = header->getNext(); if(current) return current->getVal(); return NULL;}
		H* next(){if(current){ current=current->getNext(); return current->getVal(); } return NULL;}
};

template <class H>
class BST
{
	private:
		TNode<H> *root, *current;
		int n;
	public:
		BST(){root=NULL; n=0;}
		BST<H>  *ins(H x)
		{
			TNode<H> *nd = new TNode<H>(&x);
			if(!root){ root=nd; n++; return this; }
			TNode<H> *tmp = root;
			TNode<H> *aux= new TNode<H>();
			while(tmp)
			{
				aux=tmp;	
				if(x<*tmp->getVal())
					tmp=tmp->getLeft();
				else
					tmp=tmp->getRight();
			}
			if(x<*aux->getVal())
				aux->setLeft(nd);
			else
				aux->setRight(nd);
			nd->setParent(aux);
			n++;
			return this;
		}
		
		BST<H> *canc(H x)
		{
			TNode<H> *tmp = _search(x);
			if(tmp)
			{
				_canc(tmp);
				n--;
			}
			return this;
		}
		
		BST<H> *_canc(TNode<H> *tmp)
		{
			TNode<H> *par =tmp->getParent();
			if(!tmp->getLeft() && !tmp->getRight())
			{
				if(!par) {root=NULL; return this;}
				if(par->getLeft()==tmp) par->setLeft(NULL);
				else par->setRight(NULL);
				delete tmp;
				return this;
			}
			if(tmp->getLeft() && tmp->getRight())
			{
				TNode<H> *s = _succ(tmp);
				tmp->setVal(s->getVal());
				_canc(s);
				return this;
			}
			TNode<H> *son = tmp->getLeft();
			if(!son) son = tmp->getRight();
			if(!par) root=son;
			else if(par->getLeft() == tmp ) par->setLeft(son);
			else par->setRight(son);
			son->setParent(par);
			delete tmp;
			return this;
		}
		
		TNode<H> *_search(H x)
		{
			TNode<H> *tmp = root;
			while(tmp && x!=*tmp->getVal())
			{
				if(x<*tmp->getVal())
					tmp=tmp->getLeft();
				else
					tmp=tmp->getRight();
			}
			return tmp;
		}
		
		H* search(H x)
		{
			TNode<H> *tmp = _search(x);
			if(tmp) return tmp->getVal();
			return NULL;
		}
		
		TNode<H> *_succ(TNode<H> *tmp)
		{
			if(tmp->getRight()) return _min(tmp->getRight());
			TNode<H> *par = tmp->getParent();
			TNode<H> *x = tmp;
			while(par && x==par->getRight())
			{
				x=par;
				par=par->getParent();
			}
			return par;
		}
		
		H *successor(H x)
		{
			TNode<H> *tmp = _search(x);
			if(tmp) return _succ(tmp)->getVal();
			return NULL;
		}
		
		TNode<H> *_min(TNode<H> *tmp)
		{
			while(tmp->getLeft())
				tmp=tmp->getLeft();
			return tmp;
		}
		H *minimum()
		{
			TNode<H> *tmp = _min(root);
			if(tmp) return tmp->getVal();
			return NULL;
		}
		
		H* begin()
		{
			current=_min(root); if(current) return current->getVal(); return NULL;
		}
		
		H* next()
		{
			if(current) {current=_succ(current); return current->getVal();} return NULL;
		}
		
		int getW(){return n;}
		
		H getZ(){return *begin();}
		
		void print()
		{
			TNode<H> *tmp = _min(root);
			while(tmp)
			{
				cout<<*tmp->getVal()<<"\t";
				tmp=_succ(tmp);
			}
			cout<<endl;
		}
};



template <class H>
class BSTList
{
	private:
		DList< BST<H> > *bstlist;
		int k;
	public:
		BSTList(int k){bstlist = new DList< BST<H> >();this->k=k;}
		BSTList<H> *ins(H x)
		{
			BST<H> *ft = bstlist->begin();
			if(ft && ft->getW()<k)
			{
				bstlist->canc(*ft);
				ft->ins(x);
				bstlist->ins(*ft);
				return this;
			}
			BST<H> *nt = new BST<H>();
			nt->ins(x);
			bstlist->ins(*nt);
			return this;
		}
		BST<H>* search(H x){
			for(BST<H>* it = bstlist->begin(); it; it = bstlist->next()){
				if(it->search(x)) return it;
			}
			return NULL;
		}
		BSTList<H> *canc(H x)
		{
			BST<H> *tmp = search(x);
			while(tmp)
			{
				tmp = search(x);
				if(tmp)
				{
					bstlist->canc(*tmp);
					tmp->canc(x);
					if(tmp->begin())
						bstlist->ins(*tmp);
				}
			}
			return this;
		}
		
		void print()
		{
			for(BST<H> *it = bstlist->begin(); it; it= bstlist->next())
				it->print();
		}
};

template <class H>
int operator > (BST<H> &a, BST<H> &b)
{
	if(a.getW()>b.getW()) return 1;
	if(a.getW() == b.getW() && a.getZ()>b.getZ()) return 1;
	return 0;
}


template <class H>
int operator == (BST<H> &a, BST<H> &b)
{
	if(a.getW()==b.getW() && a.getZ() == b.getZ()) return 1;
	return 0;
}


int main()
{
	DList<int> *l = new DList<int>();
	l->ins(3)->ins(7)->ins(1)->ins(8)->ins(2)->ins(4)->print();
	l->canc(3)->canc(9)->canc(5)->canc(1)->ins(10)->ins(5)->print();
	if(l->search(5)) cout<< "elemento 5 presente"<<endl; else cout<<"elemento 5 non presente"<<endl;
	if(l->search(3)) cout<< "elemento 3 presente"<<endl; else cout<<"elemento 3 non presente"<<endl;
	BST<int> *t = new BST<int>();
	t->ins(3)->ins(7)->ins(1)->ins(8)->ins(2)->ins(4)->print();
	t->canc(3)->canc(9)->canc(5)->canc(1)->ins(10)->ins(5)->print();
	if(t->search(5)) cout<< "elemento 5 presente"<<endl; else cout<<"elemento 5 non presente"<<endl;
	if(t->search(3)) cout<< "elemento 3 presente"<<endl; else cout<<"elemento 3 non presente"<<endl;
	int *r = t->minimum();
	if(r) cout <<"il valore piu' piccolo e' "<<*r<<endl;
	if(r=t->successor(5)) cout<<"il successore di 5 e' "<< *r<<endl;
	if(r=t->successor(3)) cout<<"il successore di 3 e' "<< *r<<endl;
	BSTList<int> *b = new BSTList<int>(4);
	b->ins(3)->ins(7)->ins(1)->ins(8)->ins(3)->ins(4)->print();
	b->ins(8)->ins(6)->ins(2)->ins(4)->ins(6)->ins(3)->print();
	b->canc(3)->canc(9)->canc(5)->canc(1)->ins(10)->ins(5)->print();

	return 0;
}