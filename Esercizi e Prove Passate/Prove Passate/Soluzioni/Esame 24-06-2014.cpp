#include <iostream>
using namespace std;

template <class A> class Node {
	private: 
			Node <A>* parent,*right, *left;
			A key;
	public:
			Node(A key) {
				parent = right = left = NULL;
				this->key = key;
			}
			void setKey(A key){ this->key = key;}
			void setParent(Node <A>* parent){ this->parent = parent; }
			void setLeft(Node <A>* left){ this->left = left;}
			void setRight(Node <A>* right){ this->right = right; }
			
			A getKey(){return this->key;}
			Node <A>* getParent(){return this->parent;}
			Node <A>* getLeft(){return this->left;}
			Node <A>* getRight(){return this->right;}
			
			bool isLeaf() { return!getRight() && !getLeft()? true : false; }
}; 

template <class T> class BST {
public:
		virtual BST<T>* insert(T *x) = 0;
	//	virtual BST<T>* del(T *x) = 0;
		virtual int search(T *x) = 0;
	//	virtual void naturalFill(T* v) = 0;
		virtual void postorderPrint() = 0;
	//	virtual void printLevel(int l) = 0;
};
template <class H> class BSTree {
	private: 
			Node <H>* root; int size;
			
			BSTree<H>* _insert(H x) {
				Node <H>* newNode = new Node <H>(x);
				Node <H>* temp = root;
				Node <H>* father = NULL;
				while(temp != NULL) {
					father = temp;
					if(temp->getKey() > x) temp = temp->getLeft();
					else temp = temp->getRight();
				}
				
				if(!root) root = newNode;
				else {
					if(father->getKey() > x) father->setLeft(newNode);
					else father->setRight(newNode);
					newNode->setParent(father); 
				}
				size++;
				return this;
			}
			
			Node <H>* _search(Node <H>* temp, H x){
				if(!temp) return NULL;
				while(temp != NULL){
					if(temp->getKey() == x) return temp;
					if(temp->getKey() > x) temp = temp->getLeft();
					else temp = temp->getRight();
				}
				return NULL;
			}
			
			void _inorder(Node <H>* temp) {
				if(!temp) return;
				_inorder(temp->getLeft());
				cout << temp->getKey() << " ";
				_inorder(temp->getRight());
			}
			
			void _postorderPrint(Node <H>* temp){
				if(!temp) return;
				_postorderPrint(temp->getLeft());
				_postorderPrint(temp->getRight());
				cout<< temp->getKey() << " ";
			}
			
			Node <H>* getMin(Node <H>* temp) {
				if(!temp)return NULL;
				while(temp->getLeft() != NULL) temp = temp->getLeft();
				return temp; 
			}
			
			Node <H>* getMax(Node <H>* temp){
				if(!temp) return NULL;
				while(temp->getRight() != NULL) temp = temp->getRight();
				return temp;
			}
			
			Node <H>* getNext(Node <H>* temp) {
				if(!temp) return NULL;
				if(temp == getMax(root)) return NULL;
				if(temp->getRight()) return getMin(temp->getRight());
				while(temp->getParent()->getKey() <= temp->getKey()) temp = temp->getParent();
				return temp->getParent(); 
			} 
			
			BSTree<H>* _del(Node <H>*r , H x) {
				Node <H>* temp = _search(r,x);
				if(!temp) return this;
				
				Node <H>* father = temp->getParent();
				if(temp->isLeaf()) {
					size--;
					if(!father) root = NULL;
					else if(father->getRight() == temp) temp->setRight(NULL);
					else temp->setLeft(NULL);
					return this;
				}
				Node <H>* son = NULL;
				if(!temp->getRight() || !temp->getLeft()) {
					size--;
					if(!temp->getRight()) son = temp->getLeft();
					else son = temp->getLeft();
					if(!father) root = son;
					else if(father->getRight() == temp) father->setRight(son);
					else father->setLeft(son);
					son->setParent(father);
					return this;
				}
				
				Node <H>* next = getNext(temp);
				temp->setKey(next->getKey());
				_del(temp->getRight(),next->getKey());
				return this;
			}
			
	public: 
			BSTree(){
				root = NULL;
				size = 0;
			}
			
			BSTree<H>* insert(H* x) {
				return _insert(*x);
			} 
			
			int search(H *x) {
				return _search(root,*x)? 1 : 0;
			}
			
			void inorder(){
				_inorder(root);
				cout << endl;
			}
			
			void postorderPrint() {
				_postorderPrint(root);
				cout << endl;
			}
			
			int _getProf(Node <H>* temp) {
				if(!temp) return -1;
				int prof = 0;
				while(temp != root) {
					prof++;
					temp = temp->getParent();
				}
				return prof+1;
			}
			
			int getProf(H x) {
				Node <H>* temp = _search(root,x);
				if(!temp) return -1;
				return _getProf(temp);
			}
			void printLevel(int level) {
				Node <H>* temp = getMin(root);
				while(temp != NULL) {
					if(_getProf(temp) == level) cout << temp->getKey() << " ";
					temp = getNext(temp); 
				}
				cout << endl;
			}
			
			int getAltezza() {
				if(!size) return-1;
				Node <H>* temp = getMin(root);
				int altezza = 0;
				while(temp != NULL){
					if(_getProf(temp) > altezza ) altezza = _getProf(temp);
					temp = getNext(temp);
				}
				return altezza+1;
			}
			
			void naturalFill(H* vector){
				Node <H>* temp = getMin(root);
				int i  = 0 ;
				while(temp != NULL) {
					temp->setKey(vector[i]);
					i++;
					temp = getNext(temp);
				}
			}
			
};

int main(){
	int MyArray[] = { 23 ,4 ,6, 8 ,12 ,21 ,5 ,9 ,7 ,3 ,16 ,2 ,24};
	int secondArray[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};	
	BSTree<int>* Alberello = new BSTree<int>();
	for(int i = 0; i < 13;i++) Alberello->insert(&MyArray[i]);
	
	Alberello->naturalFill(secondArray);
	Alberello->postorderPrint();
	cout << endl;
	Alberello->printLevel(3);
	cout << endl;
	
	int altezza = Alberello->getAltezza();
	for(int i = 0 ; i < altezza;i++) {
		Alberello->printLevel(i);
		cout << " ";
	}
	
	
}


