#include <iostream>

using namespace std;


template <class H> class Node{
	
	private:
		H key;
		int mul;
		Node<H> *parent, *left, *right;

	public:
		Node(H k, Node<H> *p=0, Node<H> *l=0, Node<H> *r=0){
			key=k;
			mul=1;
			parent=p;
			left=l;
			right=r;
		}

		void setKey(H k){
			key=k;
		}

		void setParent(Node<H> *p){
			parent=p;
		}

		void setLeft(Node<H> *l){
			left=l;
		}

		void setRight(Node<H> *r){
			right=r;
		}

		H getKey() const{
			return key;
		}

		Node<H>* getParent() const{
			return parent;
		}

		Node<H>* getLeft() const{
			return left;
		}

		Node<H>* getRight() const{
			return right;
		}

		int incMul(){
			mul++;
		}

		int decMul(){
			mul--;
		}

		int getMul() const{
			return mul;
		}

		void setMul(int m){
			mul=m;
		}

		bool isLeaf(){
			return !left && !right;
		}
};


template <class H> class MultiBST{
	
	public:
		virtual MultiBST<H>* ins(H x) = 0;
		virtual int multiplicity(H x) = 0;
		virtual void inorder() = 0;
		virtual MultiBST<H>* del(H x) = 0;
		virtual int rank(H x) = 0;
};


template <class H> class MyMultiBST{
	
	private:
		Node<H>* root;

		Node<H>* _search(H x){
			Node<H>* aux=root;
			while(aux){
				if(x < aux->getKey()){
					aux=aux->getLeft();
				}
				else if(x > aux->getKey()){
					aux=aux->getRight();
				}
				else{
					return aux;
				}
			}
			return 0;
		}

		void _inorder(Node<H>* aux){
			if(aux){
				_inorder(aux->getLeft());
				for(int i=0; i < aux->getMul(); i++){
					cout<<aux->getKey()<<" ";
				}
				_inorder(aux->getRight());
			}
		}

		Node<H>* _minimum(Node<H>* aux){
			while(aux && aux->getLeft()){
				aux=aux->getLeft();
			}
			return aux;
		}

		void _del(Node<H>* aux){
			if(aux){

				//caso 1: il nodo è una foglia
				if(aux->isLeaf()){
					Node<H>* parent=aux->getParent();
					if(parent){
						if(parent->getLeft() == aux){
							parent->setLeft(0);
						}
						else{
							parent->setRight(0);
						}
						aux->setParent(0);
					}
					else{
						root=0;
					}
					delete aux;
				}

				//caso 2: il nodo ha un solo figlio
				else if(aux->getLeft()==0 ^ aux->getRight()==0){
					Node<H>* son;
					if(aux->getLeft()){
						son=aux->getLeft();
					}
					else{
						son=aux->getRight();
					}
					Node<H>* parent=aux->getParent();
					if(parent){
						if(parent->getLeft()==aux){
							parent->setLeft(son);
						}
						else{
							parent->setRight(son);
						}
						son->setParent(parent);
					}
					else{
						root=son;
					}
					delete aux;
				}

				//caso 3: il nodo ha due figli
				else{
					Node<H>* min=_minimum(aux->getRight());
					aux->setKey(min->getKey());
					aux->setMul(min->getMul());
					_del(min);
				}
			}
		}

		Node<H>* _successor(Node<H>* aux){
			if(!aux){
				return 0;
			}

			if(aux->getRight()){
				return _minimum(aux->getRight());
			}

			Node<H>* parent=aux->getParent();
			while(parent && parent->getRight() && parent->getRight() == aux){
				aux=parent;
				parent=parent->getParent();
			}
			return parent;
		}

	public:
		MyMultiBST(){
			root=0;
		}

		MyMultiBST<H>* ins(H x){
			if(!root){
				root=new Node<H>(x);
				return this;
			}

			Node<H>* tmp=_search(x);
			if(tmp){
				tmp->incMul();
				return this;
			}

			Node<H>* aux=root;
			while(true){
				if(x < aux->getKey()){
					if(!aux->getLeft()){
						Node<H>* current=new Node<H>(x, aux);
						aux->setLeft(current);
						return this;
					}
					else{
						aux=aux->getLeft();
					}
				}
				else{
					if(!aux->getRight()){
						Node<H>* current=new Node<H>(x, aux);
						aux->setRight(current);
						return this;
					}
					else{
						aux=aux->getRight();
					}
				}
			}
		}

		MyMultiBST<H>* del(H x){
			Node<H>* aux=_search(x);
			//cout<<"DEL aux: "<<aux->getKey()<<endl;
			int m=aux->getMul();
			//cout<<"DEL m: "<<m<<endl;
			if(m==1){
				//cout<<"DEL if"<<endl;
				_del(aux);
			}
			else{
				//cout<<"DEL else"<<endl;
				aux->decMul();
				//cout<<"DEL aux mul: "<<aux->getMul()<<endl;;
				//cout<<"DEL else aux: "<<aux->getKey()<<endl;
			}
			return this;
		}

		void inorder(){
			_inorder(root);
			cout<<endl;
		}

		int multiplicity(H x){
			Node<H>* aux=_search(x);
			return aux ? aux->getMul() : 0;
		}

		int rank(H x){
			Node<H>* current=_search(x);
			if(!current){
				return 0;
			}

			Node<H>* aux=_minimum(root);
			int r=0;
			while(aux!=current){
				r+=aux->getMul();
				aux=_successor(aux);
			}
			return (r+1);
		}
};

int main(){

	MyMultiBST<int>* b=new MyMultiBST<int>();
	b->ins(10)->ins(7)->ins(7)->ins(23)->ins(30)->ins(4)->ins(1)->ins(5)->ins(9)->ins(5)->ins(1)->ins(7)->ins(1)->ins(9);
	b->inorder();
	cout<<"mul 7: "<<b->multiplicity(7)<<endl;
	cout<<"mul 1: "<<b->multiplicity(1)<<endl;
	cout<<"mul 5: "<<b->multiplicity(5)<<endl;
	cout<<"mul 30: "<<b->multiplicity(30)<<endl;
	cout<<"mul 8: "<<b->multiplicity(8)<<endl;

	b->del(7)->del(4)->del(23)->del(7)->del(7);
	b->inorder();
	cout<<"mul 5: "<<b->multiplicity(5)<<endl;
	cout<<"mul 7: "<<b->multiplicity(7)<<endl;

	cout<<"rank 5: "<<b->rank(5)<<endl;
	cout<<"rank 9: "<<b->rank(9)<<endl;
	cout<<"rank 30: "<<b->rank(30)<<endl;
}