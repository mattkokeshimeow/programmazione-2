#include <iostream>

using namespace std;

template <class H> class Node{
	
	private:
		H key;
		Node<H> *next, *prev;

	public:
		Node(H k, Node<H> *n=0, Node<H> *p=0) : key(k), next(n), prev(p) {}

		void setKey(H k){
			key=k;
		}

		void setNext(Node<H>* n){
			next=n;
		}

		void setPrev(Node<H>* p){
			prev=p;
		}

		H getKey() const{
			return key;
		}

		Node<H>* getNext() const{
			return next;
		}

		Node<H>* getPrev() const{
			return prev;
		}

};

template <class H> class OrderedList{
	
	private:
		Node<H> *head, *tail;

		Node<H>* _search(H x){
			Node<H>* aux=head;

			while(aux && x >= aux->getKey()){
				if(x == aux->getKey()){
					return aux;
				}
				aux=aux->getNext();
			}

			return 0;
		}

	public:
		OrderedList(){
			head=tail=0;
		}

		OrderedList<H>* ins(H x){
			if(!head){
				head=new Node<H>(x);
				tail=head;
				return this;
			}

			//inserimento in testa
			if(x < head->getKey()){
				Node<H>* aux=new Node<H>(x, head);
				head->setPrev(aux);
				head=aux;
				return this;
			}

			//inserimento in coda
			if(x > tail->getKey()){
				Node<H>* aux=new Node<H>(x, 0, tail);
				tail->setNext(aux);
				tail=aux;
				return this;
			}

			//inserimento standard
			Node<H>* aux=head;
			while(aux && x > aux->getKey()){
				aux=aux->getNext();
			}
			if(aux){
				Node<H>* current=new Node<H>(x, aux, aux->getPrev());
				aux->getPrev()->setNext(current);
				aux->setPrev(current);
				return this;
			}
		}

		void print(){
			Node<H>* aux=head;
			while(aux){
				cout<<aux->getKey()<<" ";
				aux=aux->getNext();
			}
			cout<<endl;
		}

		int search(H x){
			Node<H>* current=_search(x);

			return current ? 1:0;
		}

		OrderedList<H>* del(H x){
			Node<H>* aux=_search(x);

			if(!aux){
				return this;
			}

			//eliminazione in testa
			if(aux == head){
				head=head->getNext();
				head->setPrev(0);
				delete aux;
				return this;
			}

			//eliminazione in coda
			if(aux == tail){
				tail=tail->getPrev();
				tail->setNext(0);
				delete aux;
				return this;
			}

			//eliminazione standard
			aux->getPrev()->setNext(aux->getNext());
			aux->getNext()->setPrev(aux->getPrev());
			delete aux;
			return this;
		}
};


template <class H> class Array{
	
	private:
		H* v;
		int size;
		int n;

		bool _isFull(){
			return n==size;
		}

		bool _isEmpty(){
			return n==0;
		}

		int _search(H x){
			if(!_isEmpty()){
				for(int i=0; i<n; i++){
					if(v[i] == x){
						return i;
					}
				}
			}
			return -1;
		}

		void _mergesort(int left, int right){

			if(left == right){
				return;
			}

			int center=(left+right)/2;

			if(n > 1){

				_mergesort(left, center);
				_mergesort(center+1, right);

				H* b=new H[n];

				int i=left;
				int j=center+1;
				int k=0;

				while(i<=center && j<=right){
					if(v[i] < v[j]){
						b[k++]=v[i++];
					}
					else{
						b[k++]=v[j++];
					}
				}

				while(i<=center){
					b[k++]=v[i++];
				}
				while(j<=right){
					b[k++]=v[j++];
				}

				for(int t=left, s=0; t<=right; t++, s++){
					v[t]=b[s];
				}
			}
		}


	public:
		Array(int s){
			size=s;
			n=0;
			v=new H[size];
		}

		int getN() const{
			return n;
		}

		void ins(H x){
			if(!_isFull()){
				v[n]=x;
				n++;
			}
		}

		void print(){
			if(!_isEmpty()){
				for(int i=0; i<n; i++){
					cout<<v[i]<<" ";
				}
			}
			cout<<endl;
		}

		int search(H x){
			int pos=_search(x);
			return pos!=-1 ? 1:0;
		}

		void del(H x){
			int pos=_search(x);
			if(!_isEmpty()){
				if(pos!=-1){
					v[pos]=v[n-1];
					n--;
				}
			}
		}

		void MergeSort(){
			_mergesort(0, n);
		}
};


int main(){
	OrderedList<int>* l=new OrderedList<int>();

	l->ins(7)->ins(3)->ins(2)->ins(5)->ins(6)->ins(1)->ins(13)->ins(4)->print();

	cout<<l->search(3)<<endl;
	cout<<l->search(10)<<endl;

	l->del(6)->del(3)->print();

	Array<int>* a=new Array<int>(13);

	a->ins(4);
	a->ins(7);
	a->ins(12);
	a->ins(10);
	a->ins(2);
	a->ins(5);
	a->ins(8);
	a->ins(9);
	a->ins(6);
	a->ins(3);
	a->ins(15);
	a->ins(22);
	a->ins(1);
	a->print();

	cout<<a->search(15)<<endl;
	cout<<a->search(25)<<endl;

	a->del(8);
	a->del(7);
	a->print();

	a->MergeSort();
	a->print();
}