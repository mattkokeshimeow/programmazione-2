#include <iostream>

using namespace std;

class Rettangolo{
	public:
		double area();

		void modifica(double,double);

		void stampa();

	private:
		double base;
		double altezza;



};

void Rettangolo :: modifica(double base, double altezza){
	this->base=base;
	this->altezza=altezza;
}

double Rettangolo :: area(){ return base*altezza; }

void Rettangolo :: stampa(){
		cout<<"Base : "<<base<<" Altezza : "<<altezza<<endl;
}

int main(){
	Rettangolo r;
	r.stampa();
	r.modifica(5,7);
	r.stampa();
	cout<<"Area : "<<r.area()<<endl;


	return 0;
}