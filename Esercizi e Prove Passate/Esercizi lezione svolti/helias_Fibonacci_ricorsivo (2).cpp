/*
  Scrivere un algoritmo che scriva N caratteri 
  della sequenza di Fibonacci in modo ricorsivo

  Coded By Helias
*/

#include <iostream>
using namespace std;

// funzione ricorsiva
int fibonacci(int n)
{
	if (n >= 2)
		return fibonacci(n-1) + fibonacci(n-2); // passo induttivo
	else
		return n; // caso base
}

int main()
{
	int n;
	cout << "Quanti caratteri di fibonacci vuoi generare?" << endl;
	cin >> n;

	for(int i = 0; i < n; i++)
	{
		cout << fibonacci(i) << " ";
	}
	cout << endl;
	return 0;
}
