#include <iostream>
#include <fstream>
using namespace std;

bool sottosequenza(string x, string y){
    int contatore = 0;
    for(int j = 0; j < y.length(); j++){
            if(x[contatore] == y[j]){
                contatore++;
                if(contatore == x.length())
                    return true;
            }
            else
            {
                contatore = 0;
                if(x[contatore] == y[j]){
                	contatore++;
                	if(contatore == x.length())
                    	return true;
            	}
            }
    }
    return false;
}

int main(){

    ifstream in("input.txt");
    ofstream file("output.txt");

    for(int i = 0; i < 100; i++){
        int n;
        string x;
        string y;
        in >> n;
        in >> x;
        for(int j = 0; j < n; j++){
            in >> y;
            if(sottosequenza(x,y))
                file << y << " ";
        }
        file << endl;
    }

}