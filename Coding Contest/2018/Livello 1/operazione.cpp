#include <iostream>
#include <fstream>
using namespace std;

void soluzione(string x, ofstream& out){

    bool trovato = false;
    int i = 1;
    while(!trovato){
        if(x[i] >= '0' && x[i] <= '9')
            i++;
        else 
            trovato = 1;
    }
    string tmp1 = x.substr(0, i);
    int val1 = stoi(tmp1);
    string tmp2 = x.substr(i+1, x.length() - 1);
    int val2 = stoi(tmp2);

    if(x[i] == '+')
        out << val1+val2;
    else if(x[i] == '-')
        out << val1-val2;
    else
        out << val1*val2;
    out << " ";
}
int main(){

    ifstream in("input.txt");
    ofstream out("output.txt");

   for(int i = 0; i < 100; i++){

        int n; in >> n;

        for(int j= 0; j < n; j++){
           string x; in >> x;
            soluzione(x, out);
        }
        out << endl;

   }



    return 0;
}