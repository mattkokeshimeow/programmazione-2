#include <iostream>
#include <fstream>

using namespace std;


void ordina(int*vett, int n){
    for(int i = 0 ; i < n - 1; i++)
        for(int j = i+1; j < n; j++)
            if(vett[i] > vett[j])
                swap(vett[i], vett[j]);
}

int minimo(int* &vett, int n){
    ordina(vett, n);
    for(int i = 0; i < n; i++)
        if(vett[i] != -1)
            return i;
}

int massimo(int* &vett, int n){
    ordina(vett, n);
    return n-1;
}

int vuoto(int*vett, int n){
    for(int i = 0; i < n; i++)
        if(vett[i] == -1)
            return i;
}

bool vettorevuoto(int*vett, int n){
    for(int i = 0; i < n; i++)
        if(vett[i] != -1)
            return false;
    return true;
}



void soluzione(int*vett, int n, ofstream &out){

    int* A = vett;
    ordina(A,n);
    int* B = new int[n];

    for(int i = 0; i < n; i++)
        B[i] = -1;

    bool finito = false;
    int sum = 0;
    while(!finito){
        if(vettorevuoto(A, n))
            finito = true;
        else{
            ordina(A,n);
            //Sposto min1,min2 in B
            B[vuoto(B,n)] = A[minimo(A,n)];
            A[minimo(A,n)] = -1;
            B[vuoto(B,n)] = A[minimo(A,n)];
            sum += A[minimo(A,n)];
            A[minimo(A,n)] = -1;
            
            if(vettorevuoto(A,n))
                finito = true;
            if(!finito){
                A[vuoto(A,n)] = B[minimo(B,n)];
                sum += B[minimo(B,n)];
                B[minimo(B,n)] = -1;
                ordina(A,n);
                
                B[vuoto(B,n)] = A[massimo(A,n)];
                sum += A[massimo(A,n)];
                A[massimo(A,n)] = -1;
                ordina(A,n);
                B[vuoto(B,n)] = A[massimo(A,n)];
                A[massimo(A,n)] = -1;
                ordina(A,n);
                
                if(vettorevuoto(A,n))
                    finito = true;
                else{
                    A[vuoto(A,n)] = B[minimo(B,n)];
                    sum += B[minimo(B,n)];
                    B[minimo(B,n)] = -1;
                    
                }
                
            }
        }
    }
    out << sum << endl;

}

int main(){

    ifstream in("input.txt");
    ofstream out("output.txt");

    for(int i = 0; i < 100; i++){
        int n;
        in >> n;
        int*vett = new int[n];
        for(int j = 0; j < n; j++)
            in >> vett[j];
            
        
        
        soluzione(vett, n, out);
    }


    return 0;
}