#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

int checkwin(int** matrix){
    if(matrix[0][0] != 0)
        if(matrix[0][0] == matrix[0][1] && matrix[0][1] == matrix[0][2])
            return matrix[0][0];
    if(matrix[1][0] != 0)
        if(matrix[1][0] == matrix[1][1] && matrix[1][1] == matrix[1][2])
            return matrix[1][0];   
    if(matrix[2][0] != 0)
        if(matrix[2][0] == matrix[2][1] && matrix[2][1] == matrix[2][2])
            return matrix[2][0];
    if(matrix[0][0] != 0)
        if(matrix[0][0] == matrix[1][0] && matrix[1][0] == matrix[2][0])
            return matrix[0][0];
    if(matrix[0][1] != 0)
        if(matrix[0][1] == matrix[1][1] && matrix[1][1] == matrix[2][1])
            return matrix[0][1];
    if(matrix[0][2] != 0)
        if(matrix[0][2] == matrix[1][2] && matrix[1][2] == matrix[2][2])
            return matrix[0][2];
    if(matrix[0][0] != 0)
        if(matrix[0][0] == matrix[1][1] && matrix[1][1] == matrix[2][2])
            return matrix[0][0];
    if(matrix[0][2] != 0)
        if(matrix[0][2] == matrix[1][1] && matrix[1][1] == matrix[2][0])
            return matrix[0][2];
    else
        return 0;
}


void soluzione(string* vett, int n, ofstream &out){
    int giocatore1 = 0;
    int giocatore2 = 0;
    int j = 0;
    
    

    for(int i = 0; i < n; i++){
            bool insert = false;
            int contatore = 0;

            int** matrix = new int*[3];

            for(int a = 0; a < 3; a++)
                matrix[a] = new int[3];
            for(int a = 0; a < 3; a++)
                for(int b = 0; b < 3; b++)
                    matrix[a][b] = 0;

            
            while(checkwin(matrix) == 0 && contatore < 9){
                if(!insert){
                    matrix[vett[j][0] - 48][vett[j][1] - 48] = 1;
                    insert = true;
                }
                else{
                    matrix[vett[j][0] - 48][vett[j][1] - 48] = 2;
                    insert = false;
                }
                contatore++;
                j++;
            }
            if(checkwin(matrix) == 1)
                giocatore1++;
            else if(checkwin(matrix) == 2)
                giocatore2++;

    }
        
    

    out << giocatore1 << " " << giocatore2 << endl;


}




int main(){

    ifstream in("input.txt");
    ofstream out("output.txt");



    for(int i = 0; i < 100; i++){
        int n;
        in >> n;
        string x;
        getline(in, x, '\n');

      
    
        string vett[n*9];
        int k = 0;
        for(int j = 2; j < x.length(); j = j+6){
            vett[k] = x[j];
            vett[k] += x[j+2];
            k++;
        }

        soluzione(vett, n, out);
    }
       
        

        

    return 0;
}