#include <iostream>
#include <fstream>
using namespace std;





bool parola(string x, string y){


    if(x == y)
        return true;

    else if(y.length() == x.length()){
        int contatore = 0;
        for(int i = 0; i < y.length(); i++){
            if(x[i] == y[i]){
                contatore++;
            }
            else if(x[i] == y[i+1] && x[i+1] == y[i]){
                contatore = contatore + 2;
                i++;
            }
            else
                contatore = 0;
            
            if(contatore == x.length())
                return true;
        }
    }
    else if(y.length() > x.length()){
        int contatore = 0;
        for(int i = 0; i < y.length(); i++){
            if(x[contatore] == y[i]){
                contatore++;
                if(contatore == x.length())
                    return true;
            }
            else if(x[contatore] == y[i+1] && x[contatore+1] == y[i]){
                contatore += 2;
                i++;
                if(contatore == x.length())
                    return true;
            }
            else
                contatore = 0;
        }
    }
    return false;
}




void soluzione(string x, string text, ofstream& out){
    int indice;
    for(int i = 1; i < text.length();i++){

        indice = i + x.length();

        string y = "";
        for(int j = i; j < indice; j++)
            y += text[j];

        if(parola(x, y))
            out << i - 1 << " ";
    }



}

int main(){

    ifstream in("input.txt");
    ofstream out ("output.txt");


    for(int i = 0; i < 100; i++){
        string x;
        in >> x;
        string text;

        getline(in, text, '\n');
        soluzione(x, text, out);
        
        out << endl;
    }



    return 0;
}