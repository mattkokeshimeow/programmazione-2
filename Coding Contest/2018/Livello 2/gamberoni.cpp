#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;


float guadagno(float* vett, int n){
    float max = vett[1] - vett[0];
    
    for(int i = 0; i < n -1; i++){
        for(int j = i +1; j < n; j++){
            if(vett[j] - vett[i] > max)
                max = vett[j] - vett[i];
        }
    }
    return max;
}
int main(){

    ifstream in("input.txt");
    ofstream out("output.txt");

    for(int j = 0; j < 100; j++){
        int n;
        in >> n;
        float* vett = new float[n];
        for(int i = 0; i < n; i++)
            in >>vett[i];
        out << guadagno(vett, n) << endl;
    }

    return 0;
}
