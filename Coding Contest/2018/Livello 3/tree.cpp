#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;
ifstream fin("BST2.txt");
ofstream fout("output.txt");


template <class H> class Nodo{
	private:
	   Nodo<H> *padre,*left,*right;
	   H* chiave;
	 public:
	    Nodo(H *x){
			padre=left=right=NULL;
			chiave=x;
			}
			
		Nodo<H> *getPadre(){return padre;}
		Nodo<H> *getLeft(){return left;}
		Nodo<H> *getRight(){return right;}
		H *getChiave(){return chiave;}
		
		void setPadre(Nodo<H> *p){padre=p;}
		void setLeft(Nodo<H> *p){left=p;}
		void setRight(Nodo<H> *p){right=p;}
		void setChiave(H *x){chiave=x;}
};

template <class H> class Albero{
	private:
	   Nodo<H> *radice;
	public:
	    Albero(){radice=NULL;}
	    
	    void inserisci(H *key){
			Nodo<H> *x=radice , *y=NULL;
			
			while(x!=NULL){
				y=x;
				if(*key <= *(x->getChiave())) x=x->getLeft();
				else x=x->getRight();
			}
			Nodo<H> *nuovo=new Nodo<H>(key);
			nuovo->setPadre(y);
			
			if(y==NULL){
				radice=nuovo;
			}
			else if(*key <= *(y->getChiave())) y->setLeft(nuovo);
			else y->setRight(nuovo);
		}
		
		void postorder(Nodo<H> *p){
			if(p!=NULL){
				postorder(p->getLeft());
				postorder(p->getRight());
				fout<<*(p->getChiave())<<" ";
			}
		}
		
		void postorderPrint(){
			postorder(radice);
			fout<<endl;
		}
		
		void preorder(Nodo<H> *p){
		   if(p){
			   fout<<*(p->getChiave())<<" ";
			   preorder(p->getLeft());
			   preorder(p->getRight());
		   }
		}
		
		void preorderPrint(){
			preorder(radice);
			fout<<endl;
		}
		
		void inorder(Nodo<H> *p){
			if(p){
				inorder(p->getLeft());
				fout<<*(p->getChiave())<<" ";
				inorder(p->getRight());
			}
		}
		
		void inorderPrint(){
			inorder(radice);
			fout<<endl;
		}
	    
};

int main(){
	for(int righe=0;righe<100;righe++){
		int n; 
		string tipo,stampa;
		
		fin>>tipo;
		fin>>n;
		fin>>stampa;
		
		if(tipo=="int"){
			Albero<int> tree;
			int arr[n];
			int val;
			
			for(int i=0;i<n;i++) arr[i]=0;
			int k=0;
			for(int i=4;i<n+4;i++){
				if(k<n){
				fin>>val;
				arr[k]=val;
				k++;
			}
			}
			
			for(int i=0;i<n;i++){
				tree.inserisci(&arr[i]);
			}
			
			if(stampa=="preorder") tree.preorderPrint();
			if(stampa=="postorder") tree.postorderPrint();
			if(stampa=="inorder") tree.inorderPrint();
			
		}
		
		if(tipo=="char"){
			Albero<char> tree;
			char arr[n];
			char val;
			
			for(int i=0;i<n;i++) arr[i]=' ';
			
			int k=0;
			for(int i=4;i<n+4;i++){
				if(k<n){
				fin>>val;
				arr[k]=val;
				k++;
			}
		}
			
			for(int i=0;i<n;i++){
				tree.inserisci(&arr[i]);
			}
			
			if(stampa=="preorder") tree.preorderPrint();
			if(stampa=="postorder") tree.postorderPrint();
			if(stampa=="inorder") tree.inorderPrint();
			
		}
		
		if(tipo=="double"){
			Albero<double> tree;
			double arr[n];
			double val;
			
			for(int i=0;i<n;i++) arr[i]=0.0;
			
			int k=0;
			for(int i=4;i<n+4;i++){
				if(k<n){
				fin>>val;
				arr[k]=val;
				k++;
			}
		   }
			
			for(int i=0;i<n;i++){
				tree.inserisci(&arr[i]);
			}
			
			if(stampa=="preorder") tree.preorderPrint();
			if(stampa=="postorder") tree.postorderPrint();
			if(stampa=="inorder") tree.inorderPrint();
			
		}
		
		if(tipo=="bool"){
			Albero<bool> tree;
			bool arr[n];
			bool val;
			
			for(int i=0;i<n;i++) arr[i]=false;
			
			int k=0;
			for(int i=4;i<n+4;i++){
				if(k<n){
				fin>>val;
				arr[k]=val;
				k++;
			}
		}
			
			for(int i=0;i<n;i++){
				tree.inserisci(&arr[i]);
			}
			
			if(stampa=="preorder") tree.preorderPrint();
			if(stampa=="postorder") tree.postorderPrint();
			if(stampa=="inorder") tree.inorderPrint();
			
		}
		
		
	}
}
