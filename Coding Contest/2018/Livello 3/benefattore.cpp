#include <iostream>
#include <fstream>
using namespace std;


int numerocasse(float peso, int n, float* vett){
    for(int i = 0; i < n - 1; i++)
        for(int j = i+1; j < n; j++)
            if(vett[j] < vett[i])
                swap(vett[j], vett[i]); 
    float sum = 0;
    int contatore = 0;
    while(sum < peso){
        if(sum + vett[contatore] <= peso){
            sum += vett[contatore];
            contatore++;
        }
        else
            return contatore;
        
    }
    return contatore;
}

float pesomax(float peso, int n, float* vett){
    for(int i = 0; i < n - 1; i++)
        for(int j = i+1; j < n; j++)
            if(vett[j] < vett[i])
                swap(vett[j], vett[i]); 
    float sum = 0;
    int contatore = 0;
    while(sum < peso){
        if(sum + vett[contatore] <= peso){
            sum += vett[contatore];
            contatore++;
        }
        else
            return sum;
        
    }
    return sum;
}





int main(){

    ifstream in("input.txt");
    ofstream out("output.txt");

    for(int i = 0; i < 100; i++){
        float peso;
        in >> peso;
        int n;
        in >> n;
        float* vett = new float[n];
        for(int j = 0; j < n; j++)
            in >> vett[j];
        
        out << numerocasse(peso, n, vett) << " " << pesomax(peso, n, vett) << endl;
    }

    in.close();
    out.close();

    return 0;
}
