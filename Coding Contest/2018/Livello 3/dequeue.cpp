#include <iostream>
#include <fstream>
using namespace std;

template <class H>
class Nodo{
    protected:
        H elemento;
        Nodo<H>* succ;
    public:
        Nodo(H _el): elemento(_el){
            this->succ = NULL;
        }

        //Get
        H getElemento(){return elemento;}
        Nodo<H>* getSucc(){return succ;}

        //Set
        void setSucc(Nodo<H>* succ){this->succ = succ;}
};

template <class H>
class Coda{
    protected:
        Nodo<H>* testa;
    public:
        Coda(){this->testa = NULL;}
        ~Coda(){
            Nodo<H>* iter = testa;
            Nodo<H>* tmp;
            while(iter != NULL){
                tmp = iter->getSucc();
                delete iter;
                iter = tmp;
            }
        }

        void Push(H x){
            Nodo<H>* nuovo = new Nodo<H>(x);
            if(testa == NULL)
                testa = nuovo;
            else{
                Nodo<H>* iter = testa;

                while(iter->getSucc() != NULL)
                    iter = iter->getSucc();
                iter->setSucc(nuovo);
            }
        }

        void Pop(){
            Nodo<H>* tmp = testa;
            testa = testa->getSucc();
            delete tmp;
        }

        void print(ofstream &out){
            Nodo<H>* iter = testa;

            while(iter != NULL){
                out << iter->getElemento() << " ";
                iter = iter->getSucc();
            }
            out << endl;
        }
};

int main(){
    ifstream in("input.txt");
    ofstream out("output.txt");

    for(int i = 0; i < 100; i++){
        string type; in >> type;
        cout << type << endl;
        int n; in >> n;

        if(type == "int"){
            Coda<int>* t = new Coda<int>();
            for(int j = 0; j < n; j++){
                string tmp; in >> tmp;
                
                if(tmp == "dequeue")
                    t->Pop();
                else{
                    string _value = tmp.substr(1, tmp.length());
                    int val = stoi(_value);
                    t->Push(val);
                }
            }
            t->print(out);
            delete t;
        }
        else if(type == "double"){
            Coda<double>* t = new Coda<double>();
            for(int j = 0; j < n; j++){
                string tmp; in >> tmp;
                
                if(tmp == "dequeue")
                    t->Pop();
                else{
                    string _value = tmp.substr(1, tmp.length());
                    double val = stod(_value);
                    t->Push(val);
                }
            }
            t->print(out);
            delete t;
        }
        else if(type == "bool"){
            Coda<bool>* t = new Coda<bool>();
            for(int j = 0; j < n; j++){
                string tmp; in >> tmp;
              
                if(tmp == "dequeue")
                    t->Pop();
                else{
                    string _value = tmp.substr(1, tmp.length());
                    bool val = stoi(_value);
                    t->Push(val);
                }
            }
            t->print(out);
            delete t;
        }
        else if(type == "char"){
            Coda<char>* t = new Coda<char>();
            for(int j = 0; j < n; j++){
                string tmp; in >> tmp;
                
                if(tmp == "dequeue")
                    t->Pop();
                else{
                    string val = tmp.substr(1, tmp.length());
                    t->Push(val[0]);
                }
            }
            t->print(out);
            delete t;
        }
        
    }
}