# Guardie e ladri

Per un colpo di vento, la persiana sbatacchiò. No, il Commissario Montalbano, quella finestra, non l'avrebbe chiusa manco con l'ordine del Padreterno. Per colpa di quella finestra c'era stata pochi, giorni avanti, un'azzuffatina con Livia:
- No, Salvo, di notte la finestra aperta, no! -
- Non dirmi che tu, a Boccadasse, non dormi con la finestra aperta. -
- Certo. Ma io abito al terzo piano, intanto, e poi a Boccadasse non ci sono i ladri che ci sono qua -
E così, quando una notte Livia, sconvolta, gli aveva telefonato dicendogli che, mentre era fuori, i ladri a Boccadasse le avevano svaligiato la casa, egli, dopo aver rivolto un muto ringraziamento ai ladri genovesi, era riuscito a mostrarsi dispiaciuto, ma non quanto avrebbe dovuto.
Era però vero che anche a Vigata i ladri di appartamenti non mancavano! E proprio in quei giorni il Commissario Montalbano, e il suo giovane ispettore Giuseppe Fazio, erano impegnati in una serie di appostamenti per acciuffare dei ladri di case che operavano su tutta la provincia di Vigata, e macari nella provincie vicine. Questa faccenda, però, si stava tirando per le lunghe, dato che tutti gli appostamenti svolti fino ad ora erano finiti a un nulla di fatto.
Orazio Genco, amico del Commissario, che aveva anche lui lavorato a lungo come ladro di case, soprattutto nelle zone di mare, non aveva lasciato del tutto il giro e manteneva delle comode conoscenze tra i suoi vecchi colleghi. Genco, che non amava molto quei tipi loschi, dalle maniere violente e poco raccomandabili, disse al suo amico commissario che aveva saputo da degli amici, che avevano saputo da altri amici, che la prossima rapina si sarebbe svolta, da li a poche ore, in una villetta di Licata.
- Contrada Piano Cannelle, 44. Lei dovrebbe sapere dove si attrova - gli disse Orazio Genco al telefono.
Si, il commissario conosceva bene quella casa. Una bella villa ottocentesca, appena fuori dal paese, ma distante diverse ore di macchina da Vigata. Chiamò di prescia l'ispettore Fazio e si misero in pochi minuti in strada, sulla sua vecchia Fiat Tipo, per raggiungere il luogo della rapina prima che i ladri potessero scappare.

## Specifiche
Si aiutino il Commissario Montalbano e l'ispettore Fazio a pianificare il minor numero di pause rifornimento avendo a disposizione N stazioni di servizio lungo la strada che da Vigata porta a Licata. A tal fine è necessario considerare che la strada è lunga K chilometri e che la Fiat Tipo del Commissario fa al massimo M chilometri con un pieno. Si supponga inoltre che alla partenza da Vigata l'automobile del commissario abbia il serbatoio pieno.

## Dati in input
L'input è costituito da 100 righe, una per ogni task. Ogni riga del file di input contiene 3+N valori. Il primo valore rappresenta il numero N delle stazioni di rifornimento; il secondo valore, M, rappresenta la distanza massima, in chilometri, che la Fiat Tipo può percorrere con un pieno di benzina; il terzo valore, K, rappresenta la distanza da Vigata a Licata, anch'essa espressa in chilometri. Segue la sequenza degli N valori che indicano le distanze, espresse in chilomentri, delle N stazioni di servizio dal punto di partenza. Le distanze sono ordinate all'interno della sequenza in ordine crescente.

## Dati in output
Il file di output è composto da 100 righe, una per ogni task presente nel file di input. Ogni riga conterrà un unico valore intero positivo, corrispondente al minor numero di fermate che è necessario fare per non restare senza benzina durante il tragitto.

## Note
Il valore di N è sempre compreso tra 2 e 180.
Il valore di K è sempre compreso tra 8 e 1020.
Il valore di M è sempre compreso tra 2 e K.
La distanza dei vari distributori va sempre da 1 a K, la distanza tra due distributori consecutivi non supera mai M.

### Esempio
Il seguente esempio presenta un file di input, contenente 2 task, ed il corrispondente file di output.

input.txt:
2 6 10 2 4
10 30 100 1 31 33 38 62 69 93 97 98 99

output.txt
1
6
