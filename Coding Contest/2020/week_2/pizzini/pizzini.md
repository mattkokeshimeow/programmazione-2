# Codici sui pizzini

Il telefono principiò a squillare. La sua prima reazione fu di inserrare ancora di più gli
occhi, ma non funzionò, è notorio che la vista non è l'udito. Avrebbe dovuto tapparsi le orecchie, ma preferì infilare la testa sotto il cuscino. Niente: debole, lontano, lo squillo insisteva. Si susì santiando, andò nell'altra càmmara, sollevò ìl ricevitore.
Era Fazio, già in piedi di primo matino. Il commissario gli spiò se fosse in ufficio.
- Nonsi, dottore. Hanno ammazzato a uno - rispose Fazio dall'altro capo dell'apparecchio.
Di certo non si può dire che l'ammazzatina di una pirsona càpita al momento giusto: una morte è sempre una morte. Però il fatto concreto e innegabile era che Salvo Montalbano, mentre guidava alla volta del luogo in cui era avvenuta l'ammazzatina, sentiva che il
malo umore gli stava passando. Buttarsi dintra a un'indagine gli sarebbe servito per levarsi dalla testa i pinsèri tinti che aveva avuto nell'arrisbigliarsi.
Quando arrivò sul posto dovette farsi largo tra la gente. Taliò il morto. si trattava di Emanuele Sanfilippo, detto Nenè, un picciotteddro nullafacente, poco più che ventino, jeans, giubbotto, codino, orecchino. Le scarpe dovevano essergli costate un patrimonio. Per non parlare della macchina, una duetto, in cui era stato trovato morto, sparato con un solo colpo alla tempia. I soldi di certo non glieli passava la madre, pinsò il Commissario, dato che si trattava di una povirazza che campava con una pinsione di cinquecentomila mensili.
In casa del Sanfilippo i poliziotti trovarono tutto un arsenale tecnologico ultimo modello: mega televisore, parabola sul tetto, computer, collegamento a internet, telecamera, fax, diversi hard-disk. Poi, nascosti all'interno del matarazzo della cammara da letto, vennero ritrovati diversi saccheti di droga purissima - Ecco da dove prendeva i soldi il picciotteddro! - pinsò il commissario. Inoltre, in un quaderno della vittima, trovato in un cassetto, erano segnati dei numeri con a lato dei nomi: - 34 Davide Griffo, 76 Giovanni Guttadauro, 81 Nino Grifò, e così via - Ma chissà che stavano a significare quei nomi e quei numeri.
Ogni sacchetto di droga era accompagnato da un pizzino di carta nel quale era segnata una strana sequenza di 1 e di 0. Il commissario pinsò subito che si trattava di codici, collegati in qualche modo ai numeri signati sul quaderno. Bisognava però trovare il modo di associare i codici ai numeri. Una bella camurrìa da risolvere!

## Specifiche
Si aiuti il Commissario a risolvere questa camurrìa e ad associare ogni numero presente nel quaderno al relativo codice presente sui pizzini di carta. I codici sono ottenuti attraverso un algoritmo collegato alla tanto famosa successione di Fibonacci. Questo algoritmo, preso un numero intero positivo, restituisce una sequenza binaria (composta solo da 0 e 1) dove una cifra 1 nella posizione i-esima indica che l'i-esimo numero della successione di Fibonacci fa parte della somma che ricostruisce il numero. Si fornisca quindi un programma che, preso in input un numero intero, si capace di restituire in output il corrispondente codice binario associato al numero.

## Dati in input
L'input è costituito da 100 righe, una per ogni task. Ogni riga del file di input contiene un solo valore N che indica il numero associato ad un nome, così come riportato sul quaderno della vittima.

## Dati in output
Il file di output è composto da 100 righe, una per ogni task presente nel file di input. Ogni riga conterrà la sola sequenza di cifre binarie che corrisponde al numero N, così come ottenuto dall'algoritmo.

## Note
Il valore di N è sempre compreso tra 3 e 3000.
Questo algoritmo potrebbe generare più sequenze valide, per cui va restituita quella con il minor numero possibile di 1.
In questo algoritmo la sequenza di Fibonacci non inizia con 1 e 1 ma con 1 e 2.

### Esempio
Il seguente esempio presenta un file di input, contenente 3 task, ed il corrispondente file di output.

input.txt:
9
19
15

output.txt
10001
100101
010001

Spiegazione:
Nel primo caso d'esempio il numero 9 può essere ottenuto in due modi:
1 + 3 + 5 --> 1011 che contiene 3 volte la cifra '1'
1 + 8 --> 10001 che contiene 2 volte la cifra '1' ed è la soluzione da scegliere.

Nel terzo caso d'esempio il numero 15 può essere ottenuto in due modi:
- 8 + 5 + 2 --> 01011 che contiene 3 volte la cifra '1'
- 13 + 2 --> 010001 che contiene 2 volte la cifra '1' ed è la soluzione da scegliere.
