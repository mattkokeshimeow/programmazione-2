# La gita a Cefalù

Il Commissario Salvo Montalbano si era assittato da poco al tavolo, aveva appena avuto modo di ordinare uno dei suoi piatti preferiti, una bella pepata di cozze preparata alla vigatese, che venne interrotto dalla telefonata dell'ispettore Fazio che lo informava di alcune gane che lo avrebbero allontanato da quel succulento pasto. Per qualche secondo fu tentato di ignorare la chiamata, ma il suo spirito del dovere prese infine il sopravvento.
La signora Nuccia Cammarata, nonna di 8 picciriddre, aveva chiamato pochi minuti prima, quasi in lacrime, in commissariato. La donna lamentava il fatto che la più piccola delle sue nipotine, Gigetta, tardava a rientrare a casa dopo aver passato la giornata, con la classe, in gita a Cefalù. La piccolina frequentava la terza classe della scuola elementare di Vigata e non era mai successo che ritardasse così tanto per il rientro dalla scola.
Il commissario doveva prima capire dove si fosse persa Gigetta, se durante l'escursione a Cefalù o dopo il suo rientro a Vigata. A tal fine contattò le maestre presenti alla gita per capire se tutti gli studenti fossero presenti in pullman durante il viaggio di ritorno.

## Specifiche
Si aiuti il commissario Montalbano a capire se tutti gli studenti erano saliti sul pulman per il rientro dalla gita a Cefalù. Come succede sempre, è normale che gli studenti, dopo aver passato una giornata, abbiano fatto amicizia e che scatenati, siano saliti sul pullman alla rinfusa. Grazie al lavoro delle maestre è stata stilata una lista contenente, per ogni classe, il numero di studenti saliti effettivamente sul pullman. Sarà necessario verificare quindi che il numero complessivo di studenti appartenenti ad ogni classe corrisponda al numero di studenti che è poi salito sul pulman per il rientro.

## Dati in input
L'input è costituito da 100 righe, una per ogni task. Ogni riga del file di input contiene 2+N+(L*2) valori. Il primo valore rappresenta il numero N delle classi, il secondo valore L rappresenta il numero di gruppi che al ritorno salgono su un pullman. Segue la sequenza degli N valori che indicano il numero complessivo di studenti appartenenti ad ogni classe. Le rimanenti L coppie, (x,y), indicano i gruppi di studenti che salgono sul pullman, dove x indica l'indice della classe, e y rappresenta il numero di ragazzi di quella classe saliti sul pullman.

## Dati in output
Il file di output è composto da 100 righe, una per ogni task presente nel file di input. Ogni riga conterrà il valore 0 se non manca alcuno studente all'appello. In caso contrario dovrà essere stampato il numero C delle classi che non sono ancora al completo, seguito da C coppie formate da 2 interi: la classe e il numero di ragazzi di quella classe che non sono ancora saliti su alcun pullman.

## Note
Il valore di N è sempre compreso tra 2 e 100.
Il valore di L è sempre compreso tra N e 1000.
Le classi sono numerate da 0 a N-1.
E’ necessario stampare le classi nell’ordine in cui sono state lette, ovvero in ordine crescente in base all’indice.

### Esempio
Il seguente esempio presenta un file di input, contenente 2 task, ed il corrispondente file di output.

input.txt:
3 5 4 4 3 0 2 1 3 0 1 2 2 1 1
3 6 4 4 4 0 2 1 3 2 1 0 2 2 3 1 1

output.txt
2 0 1 2 1
0

