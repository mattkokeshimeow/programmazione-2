# La piramide di Gribaudo

Quel pomeriggio c'era aria di azzuffatina per Montalbano. Il vicequestore, che non l'aveva mai potuto soffrire, lo aveva convocato di prescia nel suo ufficio.
- Montalbano, glielo dico una volta per tutte in occasione dell'arrivo del nuovo Capo della Mobile, il dottor Ernesto Gribaudo. Lei avrà funzioni di supporto. Nel suo commissariato, potrà occuparsi solo di piccole cose e lasciare che delle cose grosse si occupi la Mobile nella persona del dottor Gribaudo o del suo vice - aveva sentenziato il questore dandosi un'ariata di orgoglio e di soddisfazione.
Montalbano lo conosceva bene Ernesto Gribaudo. Leggendario. Una volta, taliando il
torace di uno ammazzato con una raffica di kalashnikov, aveva sentenziato che quello era morto per dodici pugnalate inferte in rapida successione.
Ma questo dettaglio il questore non lo poteva conoscere. Il questore manco sapeva che da qualche mesata il commissario Montalbano stavo proprio investigato sul dottor Gribaudo, che pareva fosse accanosciuto in tutta la provincia, oltre che per il suo ruolo rispettabile di capo della Mobile, anche come uno dei più temibili usurai della zona. A detta di chi ci era capitato, i suoi metodi erano meschini e brutali.
Il povirazzo o la povirazza che non arrinisciva a pagare la rata dovuta entro il giorno stabilito, doveva passare per quella che il dottor Gribaudo chiamava la sua piramide del risarcimento. Quest'ultima assomigliava ad uno di quei giochi da tavolo, tipo Monopoli. La schacchiera era di forma triangolare, con una sola casella in cima ed una casella in più ad ogni livello successivo della piramide. In ognuna delle caselle della scacchiera il Gribaudo aveva segnato una quantità di denaro. Il gioco consisteva nel partire dalla cima delle piramide e scendere fino ad una casella posizionata alla base, prendendo ad ogni passo una tra due direzioni disponibili, scendere a destra o scendere a sinistra.
In questo modo il cliente avrebbe dovuto aumentare la sua rata della somma di tutte le caselle che aveva incontrato nel suo percorso. Ovviamente l'onesto capo della Mobile si sarebbe accertato che la somma ottenuta sarebbe stata sempre la massima possibile. Una maniera meschina di prendersi gioco dei suoi clienti e di arricchirsi alle loro spalle.
In base alla ricostruzione fornite dai testimoni e alle prove raccolte durante le indagini al commissario Montalbano non rimaneva altro che accertarsi delle cifre che Gribaudo era riuscito ad estorcere alle sue vittime per costringerlo al risarcimento una volta tratto in arresto.

## Specifiche
Aiuta il commissario Montalbano ad individuare la somma massima che è possibile ottenere dalla piramide di Gribaudo. Si immagini tale piramide con una casella in cima, due caselle al secondo livello, tre al terzo e, in generale, n caselle all'n-esimo livello. Ogni casella contiene al suo interno un valore numerico positivo. Da una casella a livello i è possibile spostarsi verso una delle due caselle ad essa adiacenti che si trovano al livello i+1. Individuare il cammino che permetta di ottenere una somma massima dei valori contenuti nelle caselle che compongono il cammino stesso.

## Dati in input
L'input è costituito da 100 righe, una per ogni task. Ogni riga del file di input contiene un valore N che indica l'altezza della piramide seguito da Nx(N+1)/2 elementi i quali rappresentano, in successione, i valori numerici presenti nelle righe della piramide. I valori sono elencati da quelli del primo livello fino a quelli dell'ultimo livello. Per ogni livello i valori sono presentati partendo da sinistra e procedendo verso destra.

## Dati in output
Il file di output è composto da 100 righe, una per ogni task presente nel file di input. Ogni riga conterrà la somma massima tra tutte le possibili soluzioni al gioco della piramide di Gribaudo.

## Note
Il valore di N è sempre compreso tra 1 e 12.
I valori presenti nella piramide sono sempre compresi tra 0 e 10.

### Esempio
Il seguente esempio presenta un file di input, contenente 2 task, ed il corrispondente file di output.

input.txt
3 7 0 0 3 9 2
3 42 11 13 37 41 38

output.txt
16
96
