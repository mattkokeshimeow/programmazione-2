# La guantiera di dolcetti

Ogni prima domenica d'aprile il giornalista televisivo Nicolò Zito, uno dei migliori amici del commissario Montalbano, rapriva ufficialmente la casa di campagna a Cannatello, ereditata da suo padre. In quella casa Nicolò viveva con sua mogliere Taninè e il loro figlio mascolo Francesco, che proprio quel giorno festeggiava i suoi sette anni.
Per andarci il commissario affrontava trazzere, mulattiere e polverosi viottoli che gli imbiancavano la macchina. Arrivò che era quasi l'ora di mangiare. Il profumo della guantiera di dolcetti alla ricotta che aveva accattato per il compleanno di Francesco inondava l'abitacolo e gli faceva smorcare l'appetito.
Per l'occasione il commissario aveva accattato dolci di ogni sorta: minnuzzi, cuticcheddi, traccine, cassatelle, bersaglieri, muccunetti, sispiri di monaca e tante altre prelibatezze che piacevano assai al picciriddru. Il commissario aveva notato che i dolcetti più golosi per Francesco era quelli con la pasta frolla, la glassa e ripieni di ricotta con chicchi di cioccolato.
Uno dei momenti più divertenti della giornata era quello della divisione dei dolcetti. Taninè e suo marito erano infatti soliti iniziare in primavera una rigorosa dieta e non avrebbero assaggiato neanche un coccio di quelle prelibatezze. Non rimaneva quindi per il commissario che spartirsi la guantiera con il piccolo Francesco.
In questa occasione i due erano soliti assegnare un punteggio ad ognuno dei dolcetti sulla guantiera. Poi, a turno, i due sceglievano un dolcetto alla volta per divorarlo in un sol boccone.
Come da copione, alla fine del pranzo, Taninè puliva la cucina, Nicolò tornava a lavoro nei campi con il picciriddru, e Montalbano, con ancora la zuppa di maiale e quindici dolcetti giganti sullo stomaco, si abbandonava al suo solito mal di panza.

## Specifiche
Si assuma che Francesco e Montalbano assegnino, indipendentemente, a ciascun dolcetto della guantiera un punteggio. I punteggi assegnati da Montalbano non saranno quindi necessariamente uguali a quelli assegnati da Francesco.
Si aiuti Francesco e il commissario Montalbano a dividersi i dolcetti alla ricotta in maniera che la somma complessiva dei punteggi assegnata ad ognuno dei dolcetti scelti sia massima. Più formalmente si dovrà massimizzare la somma dei punteggi assegnati da Montalbano ai dolcetti da lui scelti e la somma dei punteggi assegnati da Francesco ai dolcetti da lui scelti.

## Dati in input
L'input è costituito da 100 righe, una per ogni task. Ogni riga del file di input contiene un valore N che indica il numero di dolcetti presenti nel vassoio, seguito da N coppie composte da due interi positivi. I valori dell'i-esima coppia indicano i punteggi assegnati da Montalbano e Francesco, rispettivamente, per l'i-esimo dolcetto.

## Dati in output
Il file di output è composto da 100 righe, una per ogni task presente nel file di input. Ogni riga conterrà la massima somma possibile dei punteggi ottenuta dalla divisione dei dolcetti.

## Note
Il valore di N è sempre compreso tra 4 e 250 ed è sempre pari.
I voti dei dolcetti sono sempre interi positivi compresi tra 0 e 30.
Lo stesso dolcetto non può essere scelto sia da Francesco che da Montalbano.

### Esempio
Il seguente esempio presenta un file di input, contenente 2 task, ed il corrispondente file di output.

input.txt
8 10 6 2 6 4 1 6 0 1 3 7 8 3 5 4 7
4 9 2 6 7 1 0 30 4

output.txt
48
46

Spiegazione:
Nel secondo caso d'esempio, conviene creare un vassoietto per Montalbano con i dolcetti in posizione 0 e 3 (9+30) e un vassoietto per Francesco con i dolcetti in posizione 1 e 2 (7+0), ottenendo così la somma massima di 46.
