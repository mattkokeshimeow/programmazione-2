/*2TCap8_4.cpp. Pila statica */
#include<iostream>
using namespace std;
template<typename T>
class Stack
{
	public:
		Stack(int =3);
		~Stack();
		Stack& push(const T& );
		Stack& pop();
		void print() const ;
	private:
		T*ptr;
		int head;
		int size;
		bool isEmpty() const { return head==-1;}
		bool isFull() const {return head==size-1;}
};
template<typename T>
Stack<T>::Stack(int dim )
{
	size = ( dim<0)? 3 : dim;
	ptr=0;
	ptr = new T[size] ;
	head = -1;
	
}
template<typename T>
Stack<T>::~Stack()
{

	delete []ptr;

}

template<typename T>
Stack<T>& Stack<T>::push(const T& input)
{
	if (isFull() ==true)
		cout<<"Stack is Full!"<<endl;
	else
		{
			
			ptr[++head]= input;
		
		}

	return *this;
}
template<typename T>
Stack<T>& Stack<T>::pop()
{
	if (isEmpty() ==true)
		cout<<"Stack is Empty!"<<endl;
	else
		{
			head--;
		
		}
	return *this;
}
template<typename T>
void Stack<T>::print()const
{
	if (isEmpty() ==true)
		cout<<"Stack is Empty!"<<endl;
	else
		{
			
			
			for(int indice =head; indice>=0 ;indice--)
				cout<<"Data is : "<<ptr[indice]<<endl;
					cout<<endl<<endl;

		}
}

int main()
{
	cout<<"Pila statica"<<endl;

	Stack<int> stack0;
	stack0.push(3).push(2).push(1).push(0);
	stack0.print();
	stack0.pop().pop();
	stack0.print();
	return 0;
}
