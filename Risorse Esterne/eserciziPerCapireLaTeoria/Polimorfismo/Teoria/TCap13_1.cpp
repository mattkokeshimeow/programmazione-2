/*TCap13_1.cpp. Mostrare il polimorfismo del programma di TCap13_0.cpp  
TEORIA: Il polimorfismo si ha con il binding dinamico. Esso permette di sapere dinamicamente a quale oggetto si riferisce una certa funzione.
Supponiamo di avere la class base "Disegna" con il metodo "disegna" e le classi derivate "Quadrato","Cerchio" che ereditano da essa. Il binding
dinamico si ha creando un puntatore alla classe Disegna che viene assegnato ai puntatori delle diverse classi e con una scrittura del tipo
DisegnaPtr.disegna evoca dinamicamente la funzione disegna dell'oggetto assegnato al puntatore DisegnaPtr. Di fatto il polimorfismo permette
di sostiture la logica dello switch nel far eseguire il corretto metodo "disegna". Si ha un leggero consumo dell'efficienza ,importante solo
in certi contesti(applicazioni real time) ,ma di contro, un aumento della chiarezza e qualità del codice. 
NOTE: nello specifico per realizzare il binding dinamico si deve mettere la parola "virtual"nelle funzioni della classe base . Le classe 
derivate decideranno poi se implementare tali funzioni. Se le implementano si può sfruttare il binding dinamico. Altrimenti implementanto
i metodi della classe base . 
NOTE FINALI: Il polimorfismo conviene usarlo sfruttando ad esempio un array di puntatori a certe classi ,ad esempio "Cerchio","Rombo","Triangolo"
. Per evocare la funzione disegna basta passare al programma tale array e sfruttare il binding dinamico senza conoscere effettivamente
le proprietà degli oggetti dell'array . Diversamente bisogna usare la logica switch e valutare in base all'oggetto di fronte ed ad una sua 
proprietà,la funzione da evocare. Ad esempio un possibile binding statico sarebbe se Ptr è il puntatore collegato ad un certo elemento x dell'
array : case Ptr.tipoFigura="Triangolo" : esegui disegna deltriangolo ; case Ptr.tipoFigura ="Cerchio":esegui disegnaCerchio ecc . 
NOTE FINALI2: è possibile definire delle classi astratte con delle funzioni virtuali pure .Esse hanno luogo quando non ha senso che 
 le funzioni in questioni della classe astratta/base abbiano una implementazione. Un oggetto dove troviamo tali funzioni non può essere istanzia
 to però si possono usare riferimenti o altro per sfruttare il polimorfismo. Quindi una funzione virtual PURA non deve avere implementazione
 nella classe astratta e deve essere OBBLIGATORIAMENTE ereditata e implementata in tutte le classe che derivano dalla classe astratta. Una
 funzione si rende pura ponendo nell'interfaccia la funzione = 0. 
della classe base non 
 */
 
#include<iostream>
#include<string>
#include<iomanip>
using namespace std;
//Classe Base
class CommissionEmployee
{
	public:
		CommissionEmployee(const string&,const string &,const string &,double =0.0,double=0.0);
		
		void setFirstName(const string &);
		string getFirstName()const;
		
		void setLastName(const string &);
		string getLastName()const;
		
		void setSocialSecurityNumber(const string &);
		string getSocialSecurityNumber()const;
		
		void setGrossSales(double);//imposta il totale vendite
		double getGrossSales()const;
		
		
		void setCommissionRate(double);//imposta percentuale
		double getCommissionRate()const;
		
	virtual double earnings()const;
	virtual	void print() const;
		
	private:
		string firstName;
		string lastName;
		string socialSecurityNumber;
		double grossSales;//totale vendite
		double commissionRate;//percentuale sulle vendite
		
};

CommissionEmployee::CommissionEmployee(const string &first,const string &last,const string & ssn,
double sales,double rate):firstName(first),lastName(last)
{
	/*firstName = first;
	lastName =last;*/
	socialSecurityNumber = ssn;
	setGrossSales(sales);
	setCommissionRate(rate);
	

}
void CommissionEmployee::setFirstName(const string &first)
{
	firstName = first;
}
string CommissionEmployee::getFirstName()const 
{
	return firstName;
}
void CommissionEmployee::setLastName(const string &last)
{
	lastName = last;
}
string CommissionEmployee::getLastName() const
{
	return lastName;
}
void CommissionEmployee::setSocialSecurityNumber(const string &ssn)
{
	socialSecurityNumber = ssn;

}
string CommissionEmployee::getSocialSecurityNumber()const
{
	return socialSecurityNumber;
}
void CommissionEmployee::setGrossSales(double sales)
{
	grossSales = (sales<0.0)?0.0:sales;
} 
double CommissionEmployee::getGrossSales()const 
{
	return grossSales;
}
void CommissionEmployee::setCommissionRate(double rate)
{
	commissionRate = (rate>0.0 &&rate<1.0)? rate : 0.0;
}

double CommissionEmployee::getCommissionRate()const
{
	return commissionRate;
}
double CommissionEmployee::earnings()const
{
	return commissionRate*grossSales;
}
void CommissionEmployee::print() const
{
	cout<<"Commission Employee : "<<firstName<<' ' <<lastName
	<< "\nsocial security number:: " <<socialSecurityNumber
	<<"\ngross sales: "<<grossSales
	<<"\ncommission rate: "<<commissionRate;

}
//Classe derivata 
class BasePlusCommissionEmployee: public CommissionEmployee
{
	public:
		BasePlusCommissionEmployee(const string &,const string&,const string&,double =0.0,double =0.0,double =0.0);
		void setBaseSalary(double);//imposta il fisso mensile
		double getBaseSalary()const;
	virtual	double earnings()const; 
	virtual	void print() const;
	private:
		double baseSalary;
};
BasePlusCommissionEmployee::BasePlusCommissionEmployee(const string &first,const string &last,const string & ssn,
double sales,double rate,double bs):CommissionEmployee(first,last,ssn)//chiamata esplicita costruttore di base per inizializzare le variabili ereditate
{
	setBaseSalary(bs);

}
void BasePlusCommissionEmployee::setBaseSalary(double salary)
{
	baseSalary = (salary<0.0 )? 0.0 : salary;
}
double BasePlusCommissionEmployee::getBaseSalary()const
{
	return baseSalary;
}
double BasePlusCommissionEmployee::earnings()const//restitusce retribuzione ovvero salario + proviggione
{
	return baseSalary + CommissionEmployee::earnings(); //USO RIUTILIZZO SOFTWARE CON OPERATORE ::
}
void BasePlusCommissionEmployee::print() const
{
	//Invece di riusare il pezzo di codice commentato come già fatto basta richiamare la funzione print della classe base
	CommissionEmployee::print(); //USO RIUTILIZZO SOFTWARE CON OPERATORE ::
	/*cout<<"Commission Employee : "<<getFirstName()<<' ' <<getLastName()
	<< "\nsocial security number:: " <<getSocialSecurityNumber()
	<<"\ngross sales: "<<getGrossSales()
	<<"\ncommission rate: "<<getCommissionRate()*/
	
	cout <<"\nDato che manca nel print della classe base-> salary : " <<getBaseSalary()<<endl ;

}

int main(){


	cout<<"Polimorfismo"<<endl;
	CommissionEmployee commissionEmployee("Sue","Jones","1111",10000,.06); //oggetto della classe base
	CommissionEmployee *commissionEmployeePtr = 0; //puntatore alla classe base
	
	BasePlusCommissionEmployee basePlusCommissionEmployee("Daniela","Ricca","222",10000,.06,200); //oggetto della classe derivata
	BasePlusCommissionEmployee*basePlusCommissionEmployeePtr = 0;//puntatore alla classe derivata
	
	commissionEmployee.print();//chiama la funzione print della classe base
	commissionEmployeePtr = &commissionEmployee; //assegna indirizzo classe base a puntatore classe base
	commissionEmployeePtr->print();//chiama la funzione print della classe base
	commissionEmployeePtr= &basePlusCommissionEmployee; //assegna indirizzo classe derivata a puntatore classe base in quanto un oggetto derivato è un oggetto della classe base
	commissionEmployeePtr->print();//chiama la funzione print della classe derivata .BINDING DINAMICO,POLIMORFISMO.
	basePlusCommissionEmployeePtr = &basePlusCommissionEmployee;
	basePlusCommissionEmployeePtr->print();
	//commissionEmployeePtr->getBaseSalary();ERRORE perchè chiama una funzione che non appartiene alla classe base. Per farlo usare casting
	

	//basePlusCommissionEmployeePtr = &commissionEmployee;  ERRORE un oggetto base non è un oggetto derivato
	


return 0;
}

