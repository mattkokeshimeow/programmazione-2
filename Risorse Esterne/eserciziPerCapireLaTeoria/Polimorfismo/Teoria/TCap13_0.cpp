/*TCap13_0.cpp*/
/*POLIMORFISMO
NOTE TEORIA: 
Il polimorfismo permette di programmare in generale più che nello specifico e permette di trattare oggetti delle classi derivate come oggetti
della classe base. 
1) POLIMORFISMO VS EREDITARIETA': L'ereditarietà permette di evitare il copia e incolla ed di ereditare METODI UGUALI da una classe base . Tipo
La classe felino avrà la proprietà "coda" che sarà uguale a quella deella classe derivata "Gatto" e il metodo "muovi coda" sarà uguale e pertanto
ereditato . Tuttavia se consideriamo in un Felino il metodo " verso" probabilmente il verso di un "Gatto" sarà diverso da quello di un"Leone"
entrambi felini. Quindi il metodo "Verso" verrà ereditato dalla classe base "Felino" sia dalla classe"Gatto" che da "Leone" ma l'implementazion
e è diversa. Inoltre vorremmo che tale metodo debba per forza essere implementato e usato da tutte le classi derivate da Felino,questo perchè
vogliamo che ovviamente un qualsiasi felino abbia un certo "verso".Sarebbe utile fare in modo che un oggetto Felino in base al contesto
cioè in base alla classe a cui è collegato(Gatto,Leone,ecc)abbia un certo verso ,lo specifico verso,sfruttando un'unica chiamata di funzione
(in questo caso della funzione verso).Tale chiamata ,messaggio avrà diverse forme(polimorfismo)di risposte . 
 In questi casi si usa il polimorfismo.
 NOTE VARIE(importanti): 
 0)Ad  un puntatore della classe base può essere assegnato un puntatore alla classe derivata . In quanto un oggetto della class derivata è un 
 oggetto della classe base (non viceversa). In questo caso come si vede qui,un puntatore di questo tipo comunque può evocare SOLO metodi 
 ereditati dalla classe base . E (sempre in questo caso,non essendoci virtual) veerrà evocata comunque la funzione della classe base (anche 
 se a tale puntatore è stata assegnato l'indirizzo della classe derivata. Con le funzioni virtual sarà diverso).
 1) Se proviamo ad evocare con un puntatore del punto 0) una funzione non ereditata ma specifica della classe derivata,ci sarà un errore.
 Il rimedio è usare il downcasting che si vedrà più avanti.
 
 */
 
#include<iostream>
#include<string>
#include<iomanip>
using namespace std;
//Classe Base
class CommissionEmployee
{
	public:
		CommissionEmployee(const string&,const string &,const string &,double =0.0,double=0.0);
		
		void setFirstName(const string &);
		string getFirstName()const;
		
		void setLastName(const string &);
		string getLastName()const;
		
		void setSocialSecurityNumber(const string &);
		string getSocialSecurityNumber()const;
		
		void setGrossSales(double);//imposta il totale vendite
		double getGrossSales()const;
		
		
		void setCommissionRate(double);//imposta percentuale
		double getCommissionRate()const;
		
		double earnings()const;
		void print() const;
		
	private:
		string firstName;
		string lastName;
		string socialSecurityNumber;
		double grossSales;//totale vendite
		double commissionRate;//percentuale sulle vendite
		
};

CommissionEmployee::CommissionEmployee(const string &first,const string &last,const string & ssn,
double sales,double rate):firstName(first),lastName(last)
{
	/*firstName = first;
	lastName =last;*/
	socialSecurityNumber = ssn;
	setGrossSales(sales);
	setCommissionRate(rate);
	

}
void CommissionEmployee::setFirstName(const string &first)
{
	firstName = first;
}
string CommissionEmployee::getFirstName()const 
{
	return firstName;
}
void CommissionEmployee::setLastName(const string &last)
{
	lastName = last;
}
string CommissionEmployee::getLastName() const
{
	return lastName;
}
void CommissionEmployee::setSocialSecurityNumber(const string &ssn)
{
	socialSecurityNumber = ssn;

}
string CommissionEmployee::getSocialSecurityNumber()const
{
	return socialSecurityNumber;
}
void CommissionEmployee::setGrossSales(double sales)
{
	grossSales = (sales<0.0)?0.0:sales;
} 
double CommissionEmployee::getGrossSales()const 
{
	return grossSales;
}
void CommissionEmployee::setCommissionRate(double rate)
{
	commissionRate = (rate>0.0 &&rate<1.0)? rate : 0.0;
}

double CommissionEmployee::getCommissionRate()const
{
	return commissionRate;
}
double CommissionEmployee::earnings()const
{
	return commissionRate*grossSales;
}
void CommissionEmployee::print() const
{
	cout<<"Commission Employee : "<<firstName<<' ' <<lastName
	<< "\nsocial security number:: " <<socialSecurityNumber
	<<"\ngross sales: "<<grossSales
	<<"\ncommission rate: "<<commissionRate;

}
//Classe derivata 
class BasePlusCommissionEmployee: public CommissionEmployee
{
	public:
		BasePlusCommissionEmployee(const string &,const string&,const string&,double =0.0,double =0.0,double =0.0);
		void setBaseSalary(double);//imposta il fisso mensile
		double getBaseSalary()const;
		double earnings()const;
		void print() const;
	private:
		double baseSalary;
};
BasePlusCommissionEmployee::BasePlusCommissionEmployee(const string &first,const string &last,const string & ssn,
double sales,double rate,double bs):CommissionEmployee(first,last,ssn)//chiamata esplicita costruttore di base per inizializzare le variabili ereditate
{
	setBaseSalary(bs);

}
void BasePlusCommissionEmployee::setBaseSalary(double salary)
{
	baseSalary = (salary<0.0 )? 0.0 : salary;
}
double BasePlusCommissionEmployee::getBaseSalary()const
{
	return baseSalary;
}
double BasePlusCommissionEmployee::earnings()const//restitusce retribuzione ovvero salario + proviggione
{
	return baseSalary + CommissionEmployee::earnings(); //USO RIUTILIZZO SOFTWARE CON OPERATORE ::
}
void BasePlusCommissionEmployee::print() const
{
	//Invece di riusare il pezzo di codice commentato come già fatto basta richiamare la funzione print della classe base
	CommissionEmployee::print(); //USO RIUTILIZZO SOFTWARE CON OPERATORE ::
	/*cout<<"Commission Employee : "<<getFirstName()<<' ' <<getLastName()
	<< "\nsocial security number:: " <<getSocialSecurityNumber()
	<<"\ngross sales: "<<getGrossSales()
	<<"\ncommission rate: "<<getCommissionRate()*/
	
	cout <<"\nDato che manca nel print della classe base-> salary : " <<getBaseSalary()<<endl ;

}

int main(){


	cout<<"Polimorfismo"<<endl;
	CommissionEmployee commissionEmployee("Sue","Jones","1111",10000,.06); //oggetto della classe base
	CommissionEmployee *commissionEmployeePtr = 0; //puntatore alla classe base
	
	BasePlusCommissionEmployee basePlusCommissionEmployee("Daniela","Ricca","222",10000,.06,200); //oggetto della classe derivata
	BasePlusCommissionEmployee*basePlusCommissionEmployeePtr = 0;//puntatore alla classe derivata
	
	commissionEmployee.print();//chiama la funzione print della classe base
	commissionEmployeePtr = &commissionEmployee; //assegna indirizzo classe base a puntatore classe base
	commissionEmployeePtr->print();//chiama la funzione print della classe base
	commissionEmployeePtr= &basePlusCommissionEmployee; //assegna indirizzo classe derivata a puntatore classe base in quanto un oggetto derivato è un oggetto della classe base
	commissionEmployeePtr->print();//!!!chiama la funzione print della classe base con i dati della classe derivata(se ci fosse stato virtual,NO).Di conseguenza manca il dato nella funzione print "	cout <<"\nbase salary :"
	basePlusCommissionEmployeePtr = &basePlusCommissionEmployee;
	basePlusCommissionEmployeePtr->print();
	//commissionEmployeePtr->getBaseSalary();ERRORE perchè chiama una funzione che non appartiene alla classe base. Per farlo usare casting
	

	//basePlusCommissionEmployeePtr = &commissionEmployee;  ERRORE un oggetto base non è un oggetto derivato
	


return 0;
}
