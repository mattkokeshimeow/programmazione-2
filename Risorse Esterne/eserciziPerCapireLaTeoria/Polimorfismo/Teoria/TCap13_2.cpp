/*TCap13_2.cpp. PROGETTO FINALE che mostra l'uso del polimorfismo e delle classi astratte. p.696. (per vedere cosa si occupa)*/
#include<iostream>
#include<string>
#include<iomanip>
#include<typeinfo>
using namespace std;
//Classe astratta
class Employee
{
	public:
		Employee(const string&,const string&,const string & );
		virtual ~Employee(){}
		void setFirstName(const string &);
		string getFirstName()const;
		
		void setLastName(const string &);
		string getLastName()const;
		
		void setSocialSecurityNumber(const string &);
		string getSocialSecurityNumber()const;
		
		virtual double earnings()const =0; //funzione virtuale pura
		virtual	void print() const;
	private:
		string firstName;
		string lastName;
		string socialSecurityNumber;
		
};

Employee::Employee(const string &first,const string &last,const string & ssn):firstName(first)
,lastName(last),socialSecurityNumber(ssn)
{

}
void Employee::setFirstName(const string &first)
{
	firstName = first;
}
string Employee::getFirstName()const 
{
	return firstName;
}
void Employee::setLastName(const string &last)
{
	lastName = last;
}
string Employee::getLastName() const
{
	return lastName;
}
void Employee::setSocialSecurityNumber(const string &ssn)
{
	socialSecurityNumber = ssn;

}
string Employee::getSocialSecurityNumber()const
{
	return socialSecurityNumber;
}
void Employee::print() const 
{
	cout<<getFirstName() << " " <<getLastName()<<"\nsocial security number: " <<getSocialSecurityNumber();
	
}
//classe derivata concreta Salariedemployee (impiegati salariati ricevono fisso settimanale indipendentemente dal numero di ore)
class SalariedEmployee:public Employee
{
	public:
		SalariedEmployee(const string &,const string &,const string&,double =0.0);
		void setWeeklySalary(double);//imposta fisso settimanale
		double getWeeklySalary()const;//restituisce fisso settimanale
		
		virtual double earnings()const;//con virtual segnaliamo che il metodo verrà riscritto con una propria implementazione
		virtual void print()const;
	private:
		double weeklySalary;
};
SalariedEmployee::SalariedEmployee(const string&first,const string &last,const string&ssn,double salary):Employee(first,last,ssn)
{
	setWeeklySalary(salary);
};
void SalariedEmployee::setWeeklySalary(double salary) 
{
	weeklySalary = (salary<0.0)? 0.0: salary ;
	
}
double SalariedEmployee::getWeeklySalary() const 
{
	return weeklySalary;
}
double SalariedEmployee::earnings() const {
	return  getWeeklySalary();
}
void SalariedEmployee::print() const 
{
	cout<<"salaried employee: " ;
	Employee::print();
	cout<< "\nweekly salary: " <<getWeeklySalary();
	
}
//classe derivata concreta HourlyEmployee ( gli impiegati pagati ad ore ) 
class HourlyEmployee: public Employee 
{
	public:
		HourlyEmployee(const string &,const string &,const string&,double =0.0,double =0.0);
		
		void setWage(double);//imposta wage = salario=retribuzione=paga
		double getWage() const;
		
		void setHours(double);
		double getHours() const; 
		
		virtual double earnings()const;//con virtual segnaliamo che il metodo verrà riscritto con una propria implementazione
		virtual void print()const;	
	private:
		double wage; //paga oraria
		double hours;	//ore lavorate nella settimana
};
HourlyEmployee::HourlyEmployee(const string&first,const string &last,const string&ssn,double hourlywage,double  hoursWorked):Employee(first,last,ssn)
{
	setWage(hourlywage);
	setHours(hoursWorked);
};
void HourlyEmployee::setWage(double hourlywage)
{
	wage = ( hourlywage<0.0? 0.0 : hourlywage);
}
double HourlyEmployee::getWage() const
{
	return wage;
}
void HourlyEmployee::setHours(double hoursWorked)
{
	hours = (( hoursWorked>=0.0) &&(hoursWorked<=24*7))? hoursWorked: 0.0;
}
double HourlyEmployee::getHours() const
{
	return hours;
}
double HourlyEmployee::earnings()const
{
	if ( getHours()<=40) //nessun straordinario
	return getWage()*getHours();
	else 
		return 40*getWage() + (( getHours() - 40 ) *getWage()*1.5);
}
void HourlyEmployee::print() const 
{
	cout<<"Hourly employee: " ;
	Employee::print();
	cout<< "\nhourly wage: " <<getWage()
	<<"\nhours worked: " <<getHours() ;
	
}
//classe derivata concreta CommissionEmployee

class CommissionEmployee : public Employee
{
	public:
		CommissionEmployee(const string&,const string &,const string &,double =0.0,double=0.0);
		
		void setGrossSales(double);//imposta il totale vendite
		double getGrossSales()const;
		
		
		void setCommissionRate(double);//imposta percentuale
		double getCommissionRate()const;
		
		virtual double earnings()const;
		virtual	void print() const;
			
	private:
		
		double grossSales;//totale vendite
		double commissionRate;//percentuale sulle vendite
		
};

CommissionEmployee::CommissionEmployee(const string &first,const string &last,const string & ssn,
double sales,double rate):Employee(first,last,ssn)
{
	setGrossSales(sales);
	setCommissionRate(rate);
	
}

void CommissionEmployee::setGrossSales(double sales)
{
	grossSales = (sales<0.0)?0.0:sales;
} 
double CommissionEmployee::getGrossSales()const 
{
	return grossSales;
}
void CommissionEmployee::setCommissionRate(double rate)
{
	commissionRate = (rate>0.0 &&rate<1.0)? rate : 0.0;
}

double CommissionEmployee::getCommissionRate()const
{
	return commissionRate;
}
double CommissionEmployee::earnings()const
{
	return commissionRate*grossSales;
}
void CommissionEmployee::print() const
{
	cout<<"Commission Employee : ";
	Employee::print();
	cout <<"\ngross sales: "<<getGrossSales()
	<<"\ncommission rate: "<<getCommissionRate();

}
//Classe derivata 
class BasePlusCommissionEmployee: public CommissionEmployee
{
	public:
		BasePlusCommissionEmployee(const string &,const string&,const string&,double =0.0,double =0.0,double =0.0);
		void setBaseSalary(double);//imposta il fisso mensile
		double getBaseSalary()const;
	virtual	double earnings()const; 
	virtual	void print() const;
	private:
		double baseSalary;
};
BasePlusCommissionEmployee::BasePlusCommissionEmployee(const string &first,const string &last,const string & ssn,
double sales,double rate,double bs):CommissionEmployee(first,last,ssn,sales,rate)//chiamata esplicita costruttore di base per inizializzare le variabili ereditate
{
	setBaseSalary(bs);

}
void BasePlusCommissionEmployee::setBaseSalary(double salary)
{
	baseSalary = (salary<0.0 )? 0.0 : salary;
}
double BasePlusCommissionEmployee::getBaseSalary()const
{
	return baseSalary;
}
double BasePlusCommissionEmployee::earnings()const//restitusce retribuzione ovvero salario + proviggione
{
	return baseSalary + CommissionEmployee::earnings(); //USO RIUTILIZZO SOFTWARE CON OPERATORE ::
}
void BasePlusCommissionEmployee::print() const
{

	cout<<"base salaried " ;
	CommissionEmployee::print(); //USO RIUTILIZZO SOFTWARE CON OPERATORE ::
	
	
	cout <<"\n base salary : " <<getBaseSalary()<<endl ;

}
void virtualViaPointer(const Employee*const);//sfrutta un puntatore alla classe base per evocare le funzioni virtual dinamicamente
void virtualViaReference(const Employee &);//sfrutta un riferimento alla classe base per evocare le funzioni virtual dinamicamente

void virtualViaPointer(const Employee*const BasePtr) {

	BasePtr->print(); //sempre binding dinamico

}
void virtualViaReference(const Employee & BasePtr) {

	BasePtr.print();  //sempre binding dinamico

}
int main(){

	//crea oggetti classi derivate
	SalariedEmployee salariedEmployee("John","Smith","1111",800);
	HourlyEmployee hourlyEmployee("Karen","Price","2222",16.75,40);
	CommissionEmployee commissionEmployee("Sue","Jones","3333",10000,.06);
	BasePlusCommissionEmployee basePlusCommissionEmployee("Bob","lewis","4444",500,.04,300);
	
	//visualizza informazioni con binding statico
		cout<<"\nFunzioni virtuali chiamate con binding statico " <<endl;

	salariedEmployee.print();
	hourlyEmployee.print();
	commissionEmployee.print();
	basePlusCommissionEmployee.print();
	
	//crea vettore di puntatori alla classe base
	
	Employee *employees[4];
	//1 METODO CHE NON RICHIEDE IL PUNTO $1 OVVERO LA LIBERAZIONE DELLA MEMORIA
	/*
	
	employees[0] = &salariedEmployee; //oppure employees[0] = new 	SalariedEmployee ("John","Smith","1111",800); in questo caso in ($1) usare delete per liberare memoria
	employees[1]=&hourlyEmployee;
	employees[2]=&commissionEmployee;
	employees[3]=&basePlusCommissionEmployee;*/
	//2 METODO CHE RICHIEDE IL PUNTO $2
	employees[0] = new 	SalariedEmployee ("John","Smith","1111",800); //in questo caso in ($1) usare delete per liberare memoria
	employees[1]=new HourlyEmployee("Karen","Price","2222",16.75,40);
	employees[2]=new CommissionEmployee ("Sue","Jones","3333",10000,.06);
	employees[3]=new BasePlusCommissionEmployee ("Bob","lewis","4444",500,.04,300);

	
	//visualizza informazioni con binding dinamico
		cout<<"\nFunzioni virtuali chiamate con puntatore a classi derivate " <<endl;

	for ( int i =0; i< 4;i++)
		virtualViaPointer( employees[i]);
	cout<<"\nFunzioni virtuali chiamate con riferimento a classi derivate " <<endl;
	//visualizza informazioni con binding dinamico con riferimento a classi derivate
	for ( int  i =0; i< 4;i++)
		virtualViaReference( *(employees[i]));
	
	/*
	lavorando in modo generale attraverso il polimorfismo per individuare un oggetto dobbiamo effettuare un casting assegnando
	ad un puntatore di una classe derivata un puntatore di una classe base tramite un casting ( diversamente abbiamo detto che non si può fare).
	In questo modo possiamo evocare funzioni specifiche della classe derivata
	*/
	
		for ( int i =0; i< 4;i++)
		{
			BasePlusCommissionEmployee*derivatedPtr = dynamic_cast<BasePlusCommissionEmployee *>(employees[i]);
			if (derivatedPtr !=0)
			{
				double oldSalary = derivatedPtr->getBaseSalary(); //evoco funzione non virtual e strettamente appartenente alla classe BasePlusCommissionEmployee

				derivatedPtr->setBaseSalary(oldSalary *1.10); //salario aumentato del 10%
				cout <<"è un oggetto di BasePlusCommission"<<endl;	
			}
			else 
				cout<<"NON è un oggetto di BasePlusCommission"<<endl;
		
		}	
		
	/*----PUNTO $_1----*/
	//dopo aver creato i puntatori ,libero memeoria
	for (int i =0; i < 4;i++)
	{
		cout<<" cancellazione dell'oggetto della classe " <<typeid( *(employees[i])).name() <<endl;
		//($1) 
		delete employees[i]; //se prima nel riempiemento dell'array ,i puntatori sono stati creati allocando memoria.
		
	}

return 0;
}

