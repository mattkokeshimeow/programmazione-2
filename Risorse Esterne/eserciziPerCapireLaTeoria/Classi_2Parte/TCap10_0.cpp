/*TCap10_0.cpp. (1)Composizione ovverro oggetti che diventano membri di altre classi(2)Oggetti const 
NOTE:
0)TEORIA: La composizione è una relazione " ha un " Es una classe "Auto " ha un "volante" ,"un cofano". "volante","cofano" sono tutte classi
che si usano con la composizione e NON con l'ereditarietà che ha INVECE come relazione "è un". 
1) Una funzione const non può modificare oggetti e può richiamare solo funzioni const 
2) un oggetto costante non può richiamare funzioni non costanti
*/
#include<iostream>
#include<string>
using namespace std;
#ifndef DATE_H
#define DATE_H
class Date
{
	public:
		Date(int =1,int =1,int = 1900);
		void print() const;
	private:
		int month;
		int day;
		int year;
		int checkDay(int) const ;
		
		
};
#endif
Date::Date(int mn,int dy,int yr)
{
	if (mn>0&& mn<=12)//mesi
		month = mn;
	else
		{ 
			month=1;
			cout<<"Mese invalido :" <<month<<endl;
		}
	year = yr;
	day = checkDay(dy);
	print();
}
void Date::print() const
{
	cout<<month<<'/'<<day<<'/'<<year;

}
int Date::checkDay(int testDay) const //convalidare valore giorno in base al valore mese e anno
{
	static const int daysPerMonth[13] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
	if (testDay>0 && testDay<=daysPerMonth[month])
		return testDay;
	//test anni bisestili
	if (month==2 && testDay ==29 && (year%400==0 || (year%4==0&& year%100!=0)))
		return testDay;
	
	cout<<"Giorno invalido: " <<testDay <<endl;
	return 1 ;//valore consistente
}

#ifndef EMPLOYEE_H
#define EMPLOYEE_H

class Employee{

	public:
		Employee(const char*const  ,const char*const,const Date&,const Date&);
		void print()const;
	private:
		char firstName[25];
		char lastName[25];
		const Date birthDate;
		const Date hireDate;
	};
Employee::Employee(const char*const first,const char*const last,const Date &dateOfBirth,const Date &dateOfHire):birthDate(dateOfBirth),hireDate(dateOfHire)
{

	//copia first in firstName verificando lunghezza
	int length = strlen(first);
	length = (length<25? length:24);
	strncpy(firstName,first,length);
	firstName[length]='\0';
	//copia last in lastName verificando lunghezza
	 length = strlen(last);
	length = (length<25? length:24);
	strncpy(lastName,first,length);
	lastName[length]='\0';

}
void Employee::print() const {

	cout<<lastName<<", " <<firstName<<"Hired;";
	hireDate.print();
	cout<<" Birthday: ";
	birthDate.print();
	

}
#endif
int main(){

Date birth(27,02,1996);
Date hire(1,05,2016);
Employee manager("Ale","DiStefano",birth,hire);
manager.print();
Date lastDayOff(14,35,2000);//mese e giorno non valido
return 0;
}

