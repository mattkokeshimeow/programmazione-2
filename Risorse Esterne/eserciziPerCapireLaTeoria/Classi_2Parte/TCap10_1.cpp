/*TCap10_1.cpp . Programma che spiega le variabili e le funzioni static. 
Una variabile o funzione static è condivisa da tutte le istanze di una determinata classe (quella dove è stata dichiarata) permettendo un 
risparmio di memoria. Si può accedervi ad esse sfruttando la visibilità della classe (private,public..) . 
2)Conviene usarli per dichiarare costanti condivise per le istanze di una classe. Nel caso di array ,si dichiarano fuori con davanti il Nome
della classe seguito dall'operatore di risoluzione di scoope.
0) Le funzioni membro static non possono accedere ai dati delle funzioni membro non static
1) Le funzioni membro static di una classe esistono indipendentemente dagli oggetti creati dalla classe e quindi possono essere utilizzati
comunque anche se non è stato creato un oggetto. 
2) quando si vuole invocare una funzione membro static o una variabile static di una classe si fa usando il nome della classe seguita da ::.
3)le funzioni membro static non possono essere dichiarate const perchè così facendo significherebbe che la funzionee non può modificare l'oggetto
sul quale opera ma le funzioni static per il punto 1) esistono indipendentemente dall'oggetto creato
4)Le variabili static  possono essere inizializzate nell'interfaccia se 
sono di tipo 'int' o dati primitivi . IN caso di array devono essere piazzati fuori dalla classe(come per le variabili globali
*/
#include<iostream>
using namespace std;
#ifndef EMPLOYEE_H
#define EMPLOYEE_H
class Employee {

	public:
		Employee(const char*,const char*const);
		~Employee();
		const char *getFirstName()const;
		const char *getLastName() const;
		static int getCount();
	private:
		char *firstName;
		char *lastName;
		static int count; //volendo essendo int può essere inizializzato qui
		
};
 int Employee::count =0; /*dato static con visiblità della classe . Qui NON si mette static perchè static permette di rendere disponibile qualcosa alle istanze 
 di una certa classe dove è stato definito. Ogni cosa ( e quindi anche espressione) al di fuori delle classi deve essere disponibile a tutti
 i clienti e quindi non si mette static*/
 
 int Employee::getCount(){
 	return count;
 }
 Employee::Employee(const char*const first,const char*const last){
 	
 	firstName= new char[strlen(first) +1]; //l'1 sta per il carattere nullo finale
 	strcpy(firstName,first);
 	
 	lastName= new char[strlen(last) +1]; //l'1 sta per il carattere nullo finale
 	strcpy(lastName,last);
 	count++;
 	
 
 }
 Employee::~Employee(){
 delete []firstName ;
  delete []lastName ;
  count--;

 
 }
 const char*Employee::getFirstName() const {
 
 	//tale funzione non modifica nulla e restituisce un elemento rendendolo costante e quindi il client dovrà copiarlo per usarlo e non potrà modificarlo
 	return firstName;
 }
  const char*Employee::getLastName() const {
 
 	//tale funzione non modifica nulla e restituisce un elemento rendendolo costante e quindi il client dovrà copiarlo per usarlo e non potrà modificarlo
 	return lastName;
 }

#endif
int main (){
cout<<"Classi 2 parte "<<endl;
//Accedo a count prima della creazione dell'oggetto,posso farlo perchè le variabili static esistono indipendentemente dalla classe di riferimento
cout<< Employee::getCount()<<endl;
Employee *e1Ptr = new Employee("Ale","DiStefano");
Employee *e2Ptr = new Employee("Ele","Pace");
cout<<"Numero di dipendenti : " << e1Ptr->getCount()<<" == " <<Employee::getCount()<<endl;
delete e1Ptr;//dealloco memoria
e1Ptr = 0; //scollego il puntatore dalla zona di memoria
cout<<"Numero di dipendenti dopo eliminazione: " << Employee::getCount() <<endl ; 
	return 0;
};