#include<iostream>
using namespace std;
//somma di elementi di un vettore e max/min di un vettore ricorsivo,ricorsivo binario e iterativo 
/***somma elementi vettore****/
//iterativo
int sumIter( int*v,int n )
{
	int s =0;
	for(int i=0;i<n ; i++)	s+=v[ i ];
	return s;
}
//ricorsivo 1  : caso base : somma di un vettore con 1 elemento 
int sumRic(int*v,int n )
{
	if(n==1)return v[ n-1];
	return v[n-1] + sumRic(v,n-1 ) ;
}
//ricorsivo binario 
int sumRic2(int*v , int i ,int j )
{
	if(i>j)return 0;
	if(i==j)return v[ i ];
	int mid = i + (j-i)/2;
	return sumRic2(v,i,mid) + sumRic2(v,mid+1,j );
}
/***min di un vettore***/
//iterativo
int minV( int*v , int n )
{
	if(n==1)return v[n-1];
	int m = 0;
	for(int i = 1 ;i<n ;i++)
		if(v[i ]< v[m ] )m = i ;
	return v[ m ];
}
//ricorsivo
int minVRic(int*v , int n )
{
	if(n==1)return v[ n -1 ];
	int m = minVRic(v+1,n-1 ) ;
	return v[0]< m? v[0] : m ;
}
int minVRic2(int*v, int i  , int j )
{
	
	if( i==j)return v[ i ]; //un elemento
	int mid = i  + ( j-i)/2;
	int ml = minVRic2(v,i,mid ) ;
	int mr = minVRic2(v,mid+1,j);
	return ml<mr? ml : mr ;
}
//indice del minimo
int indexMinVRic( int*v,int n)
{
	if(n==1)return 0;
	int m = indexMinVRic(v+1,n-1 ) ;
	return v[0]< v[m+1]? 0 : m+1 ;
}
int indexMinVRic2(int*v,int i , int j)
{
	if(i==j ) return i;
	int mid= i +(j-i)/2;
	int ml = indexMinVRic2(v,i,mid) ;
	int mr = indexMinVRic2(v,mid+1,j) ;	
	return v[ml]<=v[mr]? ml : mr ;
}
//prodotto con somma
int sProduct( int x ,int n )
{
	int sum = 0;
	for(int i =0;i<n;i++)
		sum+=x;
	return sum;
}
//esp iterativo
int esp(int esp,int b)
{
	if(esp==0)return 1;
	int risultato = 1;
	for(int i = 1 ; i<=esp;i++)
		risultato= risultato*b;
	return risultato;
}
//fattoriale iterativo
int iFact(int n )
{
	int pre = n-1;
	while(pre >0)
	{
		n =n*pre;
		pre = pre -1 ;
	}
	return n;
cout<<" n! = "<<n<<endl;
}
int main()
{
		//cout<<sProduct(5,20)<<endl;
		/*int v[]={2,1,3,-2,4,3};
		int n =sizeof(v)/sizeof(int);
		cout<< sumIter(v,n)<<endl;
		cout<< sumRic(v,n)<<endl;
		cout<< sumRic2(v,0,n-1)<<endl;
		cout<< minV(v,n)<<endl;
		cout<< minVRic(v,n)<<endl;
		cout<< minVRic2(v,0,n-1)<<endl;
		cout<< v[ indexMinVRic(v,n) ] <<endl;
		cout<< v[ indexMinVRic2(v,0,n-1) ] <<endl;*/

return 0;
}