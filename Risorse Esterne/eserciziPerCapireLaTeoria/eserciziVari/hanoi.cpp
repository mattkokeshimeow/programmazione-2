#include<iostream>
using namespace std;
/*hanoi ricorsivo e hanoi iterativo */
/*PROBLEMA : 
Si hanno a disposizione tre paletti e bisogna spostare n dischi dal paletto 1 al 3 che è quello di destinazione 
ALGORITMO : 
bisogna prima spostare n-1 dischi dal paletto 1 al paletto 2 usando il 3 come paletto temporaneo; quindi rimarrà un solo disco nel paletto 1
che è più grande e che farà da base ai n-1 dischi; tale paletto verrà spostato dal 1 paletto a quello finale , il 3. A questo punto bisogna
spostare gli n-1 dischi dal 2 paletto al 3 usando il primo (cioè il primo paletto,quello di partenza ) come paletto temporaneo . 
*/
//******Ricorsivo******
void HanoiRic(int disks , char start,char end,char temp)
{
	if(disks==1)
	{
		cout<<"\n Spostare dal paletto :"<<start<<" al "<<end<<"\n";
		return;
	}
	//sposto n-1 dischi dal 1 (start) al secondo(temp),usando il terzo(end) come temporaneo
	HanoiRic(--disks,start,temp,end ) ;
	//sposto il disco rimanente dal 1 (start) al 3 ( end) 
	HanoiRic(1,start,end,temp);
	//sposto n-1 dischi dal 2( temp) al terzo(end) usando il primo (start) come temporaneo
	HanoiRic(disks,temp,end,start);	
}
//****Iterativo*** 
/*Bisogna usare uno stack */
//***Stack statico ***
template<typename T>
class Stack
{
	public:
		Stack(const int &n)
		{
			size = n ;
			ptr = new T[size];
			head= -1;
		}
		Stack<T>* push( T x )
		{
			if(isFull())return this;
			ptr[++head] = x;
			return this;
		}
		T pop()
		{
			if(isEmpty())return 0;
			T data = ptr[head--];
			return data;
		}
		bool isEmpty()const{return head==-1;}
		bool isFull()const{return head==size-1;}
		int getSize()const{return head+1;}
	private:
		int head,size;
		T *ptr;
};
//classe Hanoi crea un oggetto hanoi che rappresenta la situazione in un certo istante: tale oggetto conterrà il numero di dischi presenti,paletto iniziale,finale e temporaneo(appoggio)
class HanoiObj
{
	public:
		HanoiObj(int n , char start,char end,char temp)	
		{
				this->disks = n ; 
				this->s = start;
				this->e = end;
				this->t = temp;
		}
		int getDisks()const{return disks;}
		char getS()const{return s ;}
		char getE()const{return e;}
		char getT()const{return t;}
	private:
		int disks;
		char s,e , t;
};

void HanoiIter(int disks)
{
	Stack< HanoiObj * >*stack = new Stack< HanoiObj *>( 100);
	stack->push( new   HanoiObj( disks,'1','3','2') ) ;
	while(stack->isEmpty()==false)
	{
		HanoiObj* obj_pop = stack->pop();
		if( obj_pop->getDisks()==1) 
			cout<<"\n Spostare dal paletto :"<<obj_pop->getS()<<" al "<<obj_pop->getE()<<"\n";
		else
			{
				
				/* ---Come dovrebbero essere se si esegue il ragionamento fatto per la ricorsione. Tuttavia essendo uno stack si parte dal terzo 
				stack->push(  new   HanoiObj( obj_pop->getDisks()-1 ,obj_pop->getS(),obj_pop->getT(),obj_pop->getE() )  ) ;
				stack->push(  new   HanoiObj( 1 ,obj_pop->getS(),obj_pop->getE(),obj_pop->getT() )  ) ;
				stack->push(  new   HanoiObj(  obj_pop->getDisks()-1 ,obj_pop->getT(),obj_pop->getE(),obj_pop->getS() ) ) ;*/
				stack->push(  new   HanoiObj(  obj_pop->getDisks()-1 ,obj_pop->getT(),obj_pop->getE(),obj_pop->getS() ) ) ;
				stack->push(  new   HanoiObj( 1 ,obj_pop->getS(),obj_pop->getE(),obj_pop->getT() )  ) ;
				stack->push(  new   HanoiObj( obj_pop->getDisks()-1 ,obj_pop->getS(),obj_pop->getT(),obj_pop->getE() )  ) ;

			}
	}
}
int main()
{
	int n = 3;
    char n =2 ;
	cout<<"\n Versione ricorsiva con "<<n<<" piatti \n";
	 HanoiRic(n , '1','3','2'); cout<<endl<<endl;
	cout<<"\n Versione iterativa con "<<n<<" piatti \n";
	 HanoiIter(n); cout<<endl<<endl;
	return 0;
}