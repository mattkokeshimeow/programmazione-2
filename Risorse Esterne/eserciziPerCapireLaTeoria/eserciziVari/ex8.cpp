#include<iostream>
using namespace std;
/*PROGRAMMA PER CREARE ALBERI A PARTIRE DA UN ARRAY: 
1) la classe ArrayTree prende in input il numero di nodi max che contiene l'albero. Il primo nodo sarà la radice che partirà dalla posizione
 1 . Il figlio sx e dx di ogni nodo che si trova alla posizione i-esima ( dove i parte da 1 cioè posizione radice) avrà il figlio sx alla
 posizione i*2  e figlio dx alla posizione i*2 + 1 . 
 2. Ogni casella vuota viene considerata come tale se contiene 0 . Ciò ha un 'ulteriore conseguenza: ogni volta che inserisco un valore
 <= 0 inserirò lo stesso numero -1 . Ex se inserisco 0 inserirò -1 , se inserisco -1 inserirò -2 ecc. Pertanto , quando leggo un valore 
 dall'array se tale valore è <=0  dovrò leggere lo stesso incrementato di 1 .
 2) se il valore di input deve essere inserito in una casella che supera le dimensioni dell'array viene segnalato tale fatto e l'input non 
 viene inserito. 
 3) Il numero di elementi max nell'array sarà uguale al numero dei nodi max -1 . Questo perchè la radice parte dalla casella 1 e non 0 .
 Diversamente infatti non varrebbe la regola del i*2 e i*2 + 1 . Poiché comunque è solo una casella ciòò non influisce nell'efficienza.
 */
 
class ArrayTree
{
	public:
		ArrayTree( int n = 10  ):root(0){ numNodes = n<0 ? 10 : n  ; v= new int[numNodes+1]; for(int i=0;i<numNodes+1 ; i++)v[i]=0; }
		ArrayTree* insert( int input )
		{
			input = input <1 ? input-1 : input ; 
			if(root==0)
			{
				v[++root] = input; return this;
			}else
				{
					int index = root; 
					while(index<=numNodes && v[index]!= 0)
					{
						if( v[index] > input ) index = index*2 ;
						else index = index*2+1 ;
					}
					if(index>numNodes ) 
						cout<<"\nIt can't be possibile insert this object. Out of bonds !\n" ; 
					else
						v[index ] = input ; 
						
				} return this;
		}
		void printArray()
		{	
			for(int i = root ; i<=numNodes ; i++)
			{
				if ( v[ i ]==0) cout<<" null " ; 
				else
					{
						int increment = v[i ]<0? 1 : 0 ;
						cout<<v[i  ] + increment<<" ";
					}
				
			} cout<<endl<<endl;
		}
		int getLeft(int i )
		{
			int left = i*2 ;
			if( isInsideBonds(left)==false) return 0 ; //null 
			else return left;
		}
		int getRight(int i )
		{
			int right = i*2 +1 ;
			if(isInsideBonds(right)==false) return 0 ; //null 
			else return right;
			
		}
		bool isInsideBonds( int  i )
		{
			if ( i<1 || i>numNodes || v[ i ]==0) return false;
			return true;
		}
		int getFather( int i )
		{
			int f = 0 ;
			if( i%2==0 ) // se  è pari è il figlio dx
				f = (i-1)/2 ; 
			else f = (i/2);
			if( isInsideBonds(f)==false)return 0 ;
			return f;
		}
		int find( int x ) //restituisce l'indice in cui si trova x , 0 altrimenti
		{
			int index = root; 
			x = x<1? x-1 : x ;
			
			while(index<=numNodes && v[index]!= 0)
			{
				if(v[index]== x ) return index;
				else if( v[index] > x ) index = index*2 ;
				else index = index*2+1 ;
			}
			return 0 ;
		}
		int getMin( int index )//restituisce l'indice del  minimo dato in input l'indice del nodo 
		{
			if(isInsideBonds(index)==false)return 0;
			int min = index;
			while(index<=numNodes && v[index]!= 0)
			{
				min = index;
			   index = index*2 ; // vai sempre a sinistra
			}
			return min ;
		}
		
		void moveToLeft( int x ) //sposta i figli sx e destro di x : il sx andrà in x ,il destro in x + 1. Lo posso fare perchè so che il nodo da eliminare ha solo un figlio (cioè x ) quindi l'altra posizione potrà essere facilmente sostituita
		{
			if(isInsideBonds( x )!=0 && getFather(x)!=0 )
			{
				int l = getLeft( x  ) ;
				int r = getRight( x ) ;
				if(l) // se esiste il figlio sx sostituirà x 
				{
					v[ x ] = v[ l ];
					moveToLeft( l );
				
				}else v[ x ] = 0 ; //tale posizione si deve cancellare
				if(r)
				{
					v[ x + 1 ] = v[ r ];
					moveToLeft( r );

				}else v[x] = 0 ;//tale posizione si deve cancellare

			}
		}
		void del( int x )
		{
			if(!root)return ;
			int toDelete = find( x ); 
			if(!toDelete)return;
			if ( getLeft( toDelete)==0 && getRight(toDelete)==0 )//il nodo trovato è una foglia
			{
					v[ toDelete ] = 0 ;	
			}else if (getLeft(toDelete)==0 || getRight(toDelete ) == 0 ) //il nodo ha un figlio ( o sx o dx ) 
			{
					int son =0;
					if(getLeft(toDelete)!=0 )
					{	
						son = getLeft(toDelete ) ; //il figlio sx del nodo da eliminare
						v[toDelete] =v[ son ]; // al posto del nodo da eliminare c'è il figlio 
					}else
						{
							son = getRight(toDelete ) ;
							v[toDelete] =v[ son ]; // al posto del nodo da eliminare c'è il figlio 
						}
						moveToLeft(son);
			}else //ha entrambi i figli sx e dx
				{
					int nodeS = getMin( getRight( toDelete ) );//nodo sostitutivo
					int temp = v[nodeS ] ; //conserva il valore che andrà a sostituire quello del nodo da eliminare
					del( v[nodeS] ) ; //eliminare 	
					v[ toDelete ] = temp; //ora posso sostituire. Diversamente ,se l'avessi fatto prima , avrei avuto due copie e non avrei potuto sfruttare in quest modo la ricorsione(				
				}
		}
		void printInOrder(){ printInOrderHelp( root); cout<<endl<<endl;}
		void printInOrderHelp( int start)  
		{
			if(start<=numNodes && v[start]!=0)
			{
				printInOrderHelp( start*2 ) ;
				int increment = v[start]<0? 1 : 0 ;
				cout<<v[start ] + increment<<" ";
				printInOrderHelp( start*2 +1 ) ;
			}
		}
	private:
		int root ;
		int numNodes;
		int*v ;
};
int  main()
{
	ArrayTree * at = new ArrayTree( 17 ) ; 
	at->insert( 6 )->insert(2 )->insert(10)->insert(4)->insert(1)->insert(3)->insert(8)->insert(12)->insert(11)
	->insert(15)->insert(7)->insert(9)->insert(-1)->insert(0)->insert(-2)->insert(5);
	at->printArray();
	at->del( 1 ) ; //cancella un nodo che ha solo uno dei due figli
	at->del(11);//ancella una foglia
	at->del(10);//cancella un nodo che ha entrambi i figli
	at->printArray();

}	