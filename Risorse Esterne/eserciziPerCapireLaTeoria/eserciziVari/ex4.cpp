/* eliminazione da una lista circolare caso con e senza l'uso del puntatore tail */
#include<iostream>
using namespace std;
template<typename T>
class Node
{
public:
		Node( T*input = NULL ):nextPtr(0),prevPtr(0){ data = input==NULL? 0 : new T( *input) ;}
		Node<T>*getNextPtr() const {return nextPtr;}
		void setNextPtr( Node<T>*const p ){ nextPtr = p ;}
		Node<T>*getPrevPtr() const {return prevPtr;}
		void setPrevPtr( Node<T>*const p ){ prevPtr = p ;}
		void setData( T*const p ) {data = p;}
		T* getData() const {return data;}
	private:
		Node<T>*nextPtr,*prevPtr;
		T* data;

};
template<typename T>
class CL
{
	public:
		CL():head(0),n(0){}
		Node<T>*getHead()const {return head;}
		void setHead( Node<T>* h ){head = h;}
		void insert( T x ); //inserisci a destra dell'elemento d'accesso
		bool del( T x); //cancella tutte le copie
		void print() {
		
			if(isEmpty())return;
			
			Node<T>*index = head->getNextPtr();
			cout<< *(head->getData() )<<" ";
			while(index!=head)
			{	cout<< *(index->getData() )<<" "; index = index->getNextPtr();
			}	
			
		}
		void delAllcopyes( T x);
		int getN()const {return n;}
		void setN ( int num){ n = num;}
		void refreshAccess();
	private:
		bool isEmpty() const {return head==0;}
		Node<T>*head;
		int n ;
};
template<typename T>
bool CL<T>::del( T x )
{
	if ( isEmpty())return false;
	
	if ( *( head->getData() )== x ) 
		{	
			

			Node<T>*toDelete  = head;

			Node<T>*prev = toDelete->getPrevPtr();
			Node<T>*next = toDelete->getNextPtr();
			head = next;
			head->setPrevPtr(prev);
			prev->setNextPtr(head);
			toDelete->setPrevPtr(0);
			toDelete->setNextPtr(0);
			delete toDelete ; 
			toDelete =0;
			if( prev==next) //!! usare sennò col print si ha segmentation fault. Bisogna sempre controllare il caso in cui si cancella l'unico elemento
				head =0;	
		}else
	{	
		Node<T>*index  = head->getNextPtr();
		Node<T>*prev = head;
		while(index!=head && * (index->getData() )!= x)
		{
			prev = index;
			index = index->getNextPtr();
		}
		if(index!=head)
		{
			Node<T>*next  = index->getNextPtr();
			prev->setNextPtr( next);
			next->setPrevPtr(prev);
			index->setPrevPtr(0);
			index->setNextPtr(0);
			delete index ; 
			index =0;
		}else return false;
		
	}
			
return del(x);
}
template<typename T>
void CL<T>::insert( T x )
{
	if(isEmpty() )
	{	head  = new Node<T>( &x);
		head->setNextPtr(head);
		head->setPrevPtr(head);
		n++;
	}else
			{
				Node<T>*next = head->getNextPtr();
				Node<T>*newNode = new Node<T>( &x);
				head->setNextPtr( newNode);
				newNode->setPrevPtr(head);
				newNode->setNextPtr( next);
				next->setPrevPtr(newNode);
				head = newNode; n++;

			}
}
//------lista normale-----
template<typename T>
class List
{
	public:
		List():head(0){};
		List<T>* insertAtFront( T x );
		List<T>* remove( T x);
		void print() const ;
	private:
		Node<T>*head;
		bool isEmpty() const { return head==0;}
		
};
template<typename T>
void List<T>::print() const 
{
	if (isEmpty()==true)
		cout<<"List is empty. So it can't be possibile print()" <<endl;
	else
		{
			Node<T>*indice =0;
			for (indice = head; indice!=NULL;indice= indice->getNextPtr() ) 
					cout<<"Data is : " <<*indice->getData()<<endl;		
		}
		cout<<"---"<<endl;
}
template<typename T>
List<T>* List<T>::insertAtFront( T x )
{
	Node<T>*newData = new Node<T>( &x);
	if ( isEmpty()==true)
		head = newData;
	else
		{
			newData->setNextPtr(head);
			head->setPrevPtr(newData);
			newData->setPrevPtr(0);
			head = newData;
		}
	return this;
}
template<typename T>
List<T>* List<T>::remove( T x )
{
	if (isEmpty() == true)
		cout<<"List is empty. It can't be possibile removeFromFront() "<<endl;
	else if ( *head->getData()==x)
		{
			Node<T>*alias = head;
			head = head->getNextPtr();
			alias->setNextPtr(0);
			alias->setPrevPtr(0);
			delete alias;
			alias =0; 
		
		}else {
				Node<T>*index = head->getNextPtr();
				Node<T>*pre = head;
				while(index!=NULL && *index->getData()!= x)
				{	
					pre = index; index = index->getNextPtr();
				}
				if(index)
				{
				Node<T>*next = index->getNextPtr();
				if(pre)
					pre->setNextPtr(next);
				if(next)
					next->setPrevPtr(pre);
				index->setNextPtr(0);
				index->setPrevPtr(0);
				delete index;
				index =0; 

				
				}else return this;
			}
	return remove( x);
}

int main()
{
	/*CL<int> cl;
	int v []={ 5,3,5,6,5,8,9};
	for(int i=0; i<7;i++)
		cl.insert( v[ i ]);
	cl.print();
	cl.del(5);
	cout<<"\nafter elimination\n";
	cl.print();*/
	List<int> ls;
	int v []={ 5,5,9};
	for(int i=0; i<3;i++)
		ls.insertAtFront( v[ i ]);
		ls.print();
		ls.remove(5);
				ls.print();

	return 1;
}