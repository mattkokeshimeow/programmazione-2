/*esercizio che crea una simulazione di come varia la coda in un ufficio postale : ogni x minuto vengono clienti ; se non c'è nessun cliente 
che si sta servendo, allora effettuare il servizio e calcolare x minuti di durata; contemporanamente potrebbero venire altri clienti che quindi,
saranno messi in coda . Gestire le entrate e le uscite dei clienti in coda*/
#include<iostream>
#include<time.h>
using namespace std;
class Node
{
	public:
		Node( const int & t =0 ):data(t),prevPtr(0),nextPtr(0){}
		Node* getPrevPtr()const {return prevPtr;}
		void setPrevPtr( Node*const p ){prevPtr = p ;}
		Node* getNextPtr()const {return nextPtr;}
		void setNextPtr( Node*const p ){nextPtr = p ;}
		int getData()const {return data;}
		void setData( const int & t ) {data = t ;}
private:
	Node*prevPtr,*nextPtr;
	int data;
	
};
class TempList
{
	public:
		TempList():head(0){}
		void insert( const int & input)
		{
			if(isEmpty())
				head = new Node(input);
			else if( head->getData()<input)
			{
				Node*newNode = new Node(input);
				newNode->setNextPtr( head);
				head->setPrevPtr(newNode);
				head = newNode;
			}else 
				{
					Node*newNode = new Node(input);
					Node*index = head->getNextPtr();
					Node*pre = head;
					while(index!=NULL && index->getData()<input)
					{
						pre = index;
						index = index->getNextPtr();
					}
					if(pre) pre->setNextPtr( newNode);
					newNode->setPrevPtr(pre);
					if(index)index->setPrevPtr(newNode);
					newNode->setNextPtr( index);
				}
		}
		void del( int x , int& num)
		{
			if(isEmpty())return ;
			if(head->getData()== x )
			{
				Node*toDelete = head;
				head=head->getNextPtr();
				if(head)head->setPrevPtr(0);
				toDelete->setNextPtr(0);delete toDelete ; toDelete = 0;
				num++;
			}else 
				{
					Node*toDelete = head->getNextPtr();
					Node*pre = head;
					while(toDelete!=NULL && toDelete->getData()<x )
					{
						pre = toDelete;
						toDelete = toDelete->getNextPtr();
					}
					if(toDelete==NULL ||  ( toDelete!=NULL &&  toDelete->getData()>x )  )return ;
					Node*next = toDelete->getNextPtr();
					if(pre)pre->setNextPtr( next);
					if(next)next->setPrevPtr(pre);
					toDelete->setNextPtr(0);
					toDelete->setPrevPtr(0);
					delete toDelete ; toDelete = 0;
					num++;
				}
				del( x , num);
		}
	private:
		bool isEmpty() const {return head==0;}
		Node*head;
};
class Queue
{
	public:
		Queue(int n = 300 )
		{
			size = n<0? 300 : n ;
			ptr = new string[ size ] ;
			head = numEle = 0 ; 
			tail = -1 ;
		}
		void enqueue( string data ) 
		{
			if(isFull())
				cout<<" Sorry but the post office is too full for an other client! Could you wait outside ,please? \n";
			else
				{
					tail = (tail+1)%size ; 
					ptr[tail] = data;
					numEle++;
				}
		}
		void print()const
		{
			if(isEmpty())return;
			int start = head%size;
			while(start!=tail)
			{
				cout<<ptr[start]<<"  ";
				start++;
			}
			cout<<ptr[start]<<"  ";

		}
		string dequeue() 
		{
			if(isEmpty() ) return "" ;
			else 
				{
					string data =ptr[ head%size];
					head++;
					numEle--;
					return data;
				}
		}
		bool isEmpty()const {return numEle == 0 ;}
		bool isFull()const {return numEle==size ;}
	private:
		int head;
		int tail ; 
		int numEle ; 
		int size;
		string *ptr;

};
class PostOffice
{
	public:
	
	PostOffice(int timeSimulation= 1 )
	{
		cas = 0 ;
	    qc = new Queue(); //coda dei clienti
	    tl = new TempList();//lista dei tempi dei clienti che arriveranno 
	 	int t = timeSimulation<0? 1 : timeSimulation ;//minuti di durata della simulazione
	 	nClients =1; //tiene conto del numero di clienti entrati
	 	int minS=0;//tiene conto dei minuti passati
	 	int supMin=getActuallyTime();//variabile di supporto. La imposto con i minuti attuali
	 	int act = supMin;//tiene conto dei minuti attuali
	 	qc->enqueue("00");
	 	char buffer[80];
	 	cout<<"\n < The  fucking post office open >"<<endl;
		strftime (buffer,80,"Time: %I:%M%p.",localtime( &timeS));
	 	cout<<"\n< The client : 00  has arrived! > "<<buffer<<endl ;	 	
	 	int toService =  1 ; //supponiamo che il primo cliente verrà servito in un minuto 
	 	while(minS!=t)
	 	{
	 		act = getActuallyTime(); //ottieni i minuti attuali	
	 		if(cas!=0 && cas%3==0)
	 			cas =0;
	 			
	 		if(toService==minS ||toService==-1)//il servizio è stato concluso
	 		{
	 			if( toService==minS )
	 			{	
	 				string name = qc->dequeue();
					strftime (buffer,80,"Time: %I:%M%p.",localtime(&timeS));
	 				cout<<"\n< Service for the client: "<<name<<" has been effected ! > "<<buffer<<endl;
	 			}
	 			setTimeToService( toService, minS );
	 		}
	 		checkAndAdd(minS );
			if(act==supMin +1 || (act==0 &&supMin==59 )  ) //significa che è passato un minuto
	 		{
	 			//aggiorno dati per la durata della simulazione
	 			minS++;
	 			supMin = act; //ora sono uguali,quando cambierà act ,allora significa che è passato un minuto
	 			int next = 1 + (rand()%4 ) ; //calcola il tempo in cui verrà un nuovo cliente
	 			cout<<"next = "<<next ;
	 			cout<<" cas ="<<cas<<endl;
	 			tl->insert(minS+next  + cas ) ; //Aggiunge un nuovo cliente alla coda quando i minuti passati saranno quelli attuali (minS) + next 
	 			
	 			cas++;
	 		} 	
			
	 		
	 	}
	}
	void setTimeToService( int & toService,int minS)
	{	
		if(qc->isEmpty()==false) //ci sono dei clienti in coda
 				{
 						int s = 1 + (rand()%4 ) ;//il tempo in cui verrà servito tale cliente
 						toService  = minS + s ;//quando il servizio sarà concluso
 				}else toService  = - 1; //nessuno da servire finora	
	}
	void checkAndAdd( int min )//controlla se nei minuti attuali  devono arrivare dei clienti. In tal caso si devono aggiungere alla coda
	{
		int toAdd = 0;
		tl->del( min , toAdd); //cancella i tempi dalla lista e conta quanti ne sono stati cancellati. Il numero corrisponde ai clienti da aggiungere alla coda
		char buffer[80];
	    time( &timeS); //assegno le informazioni alla variabile timeve sull'ora e data corrente
	    if(toAdd==0)return ;
	    		strftime (buffer,80,"Time: %I:%M%p.",localtime( &timeS) );

		 for(int i = 0; i<toAdd; i++)
		 {
		 	
		 	string name="";
		 	
		 	if( nClients == 8 )
		 		name=" Serena ";
		 	else
		 		{
		 			 name ='A'+ i +'0'+min%24 ;
		 		}
		 	cout<<"\n< The client : "<<name << " has arrived! > "<<buffer<<endl ;
		 	qc->enqueue( name ); //aggiunge nuovo cliente 
		 	nClients++;
		 }
	
	}
	
	int getActuallyTime()
	{
		time(&timeS);
		int t = localtime(&timeS)->tm_min; //tempo attuale 
		return t;
	}
	private:
		Queue* qc;
		TempList* tl;
		time_t timeS;
		int nClients;
		int cas;
};

int main()
{

	PostOffice po( 60 );
	//ora attuale 
	
}
