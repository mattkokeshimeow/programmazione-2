/*TCap11_3.cpp. NOTE TEORIA: Quando si creano tipi di dato non predefiniti ,tramite ad esempio la ridefinizione degli operatori, risulta neces
sario ridefinire anche la funzione che fornisce il casting per un certo tipo . Per convertire un oggetto di una classe in oggetto di un'altra classe
o di un tipo predefinito si utilizza un operatore di conversione (operatore di cast). Si avrà qualcosa di questo tipo:
A::operator char *() const; per creare un oggetto temporaneo di tipo Char*() a partire da un oggetto del tipo A . Si usa const perchè non 
si deve modificare l'oggetto originiario.Se ad esempio "S" è un oggetto di una classe ,quando il compilatore incontra l'espressione:
static_cast< char * > ( s) (usata per il downncasting) viene generata la chiamata S.operator char *();
NOTE PROGRAMMA : progettazione di una classe String che mostra ulteriori esempi di ridefinizione degli operatori,l'uso dell'operatore di cast
,permettendo così la manipolaizone delle stringhe (proprio come avviene grazie alla classe <string> */
#include<iostream>
#include<cstring>//per strcpy e strcat
#include<cstdlib>//per exit
#include<iomanip>
using namespace std;

class String
{
	friend ostream& operator<<(ostream&, const String & );
	friend istream& operator>>(istream&, String & );
	
	public:
		String(const char* = "");//costruttore di default,costruttore di conversione
		String(const String &);//costruttore di copia
		~String();
		const String &operator=(const String &);//operatore di assegnamento
		const String &operator+=(const String&); //operatore di concatenamento
		
		bool operator!()const ; //stringa vuota?
		bool operator==(const String &) const ; //verifica se s1 ==s2
		bool operator<(const String &) const; //verifica se s1<s2
		
		bool operator!=(const String &right) const 
		{
		
			return ! ( *this==right);
		}
		bool operator>(const String & right)const 
		{
			return right<*this;
		}
		bool operator<=(const String &right) const
		{
			
			return !(right<*this);
		}	
		bool operator>=(const String &right)const
		{
			
			return !(*this < right);
		
		}
		char& operator[](int);//operatore di indicizzazione
		char operator[](int)const;//viene restituito una copia essendo un oggetto costante
		String operator()(int ,int=0)const;//restituisce sottostring
		int getLength() const;//restituisce lunghezza string
	private:
		int length;//lunghezza stringa senza carattere terminatore
		char*sPtr;//puntatore al primo carattere della stringa interna
		
		void setString(const char*);//funzione utilità
};
/*NOTE IMPORTANTI SULLA SEGUENTE FUNZIONE: Ogni costruttore che riceve un solo argomento,può essere visto come un costruttore di conversione.
Tale costruttore converte una string char* in un oggetto String che viene assegnato all'oggetto di destinazione. SE C'è UN COSTRUTTORE DI 
CONVERSIONE NON E' NECESSARIO scrivere la ridefinizione dell'operatore di assegnamento per assegnare stringhe ordinarie a oggetti String:
'è il compilatore stesso a invocare automaticamente il costruttore di conversione per creare un oggetto String temporaneo che contenga la 
stringa data in input. Successivamente viene invocata la ridefinzione dell'operatore di assegnamento per asseggnare l'oggeto STring temporaneo
a un altro oggetto String. Inoltre NON E' POSSIBILE FARE CATENE DI CONVERSIONE implicite : si può applicare un solo costruttore di conversione
per volta . */
String::String(const char*s):length ( (s!=0)?strlen(s):0) //Viene invocato anche quando si fa qualcosa del tipo: String s1 ; s1 =" ciao"; "ciao"viene convertito automaticamente in oggetto String e poi viene invocato l'operatore di assegnazione

{
	cout<<"Costruttore di conversione ( e default) per s = : "<<s <<endl;
	setString(s);	
}
//costuttore di copia
String::String(const String &stringaToCopy ):length(stringaToCopy.length)
{
	cout<<"Costruttore di copia" <<endl;
	setString(stringaToCopy.sPtr);

}
//distruttore
String::~String()
{

	delete[] sPtr;
}
const String& String::operator=(const String&right)
{
	if (this!=&right)
	{
		delete[] sPtr;
		length = right.length;
		setString(right.sPtr);
	}else
		cout<<"Non si può assegnare la stringa a se stesso"<<endl;

	return *this;//pere assegnamenti in cascata

}
const String& String::operator+=(const String&right)
{
	size_t newLength = length + right.length;//nuova lunghezza. Nota length non include il carattere terminatore
	char*tempPtr = new char[newLength + 1]; //l'1 è per il carattere terminatore
	strcpy(tempPtr,sPtr);//copia  il contenuto sPtr in quello di tempPtr;
	strcpy(tempPtr+length,right.sPtr); 
	delete [] sPtr;//rilascia il vecchio spazio
	sPtr = tempPtr;
	length = newLength;
	return *this;

}
//stringa vuota?
bool String::operator!() const
{
	return length==0;

}
//stringhe uguali
bool String::operator==(const String&right) const
{
	return strcmp(sPtr,right.sPtr)==0;
	
}
//stringa più piccola dell'altra?
bool String::operator<(const String & right)const 
{
	return strcmp(sPtr,right.sPtr)<0 ;
	
}
char& String::operator[](int subscript)
{
	if ( subscript<0 ||  subscript>= length)
	{	
		cout<<"Out of range"<<endl;
		exit(1);
	}
	return sPtr[subscript];
}
char String::operator[](int subscript) const
{
	if ( subscript<0 ||  subscript>= length)
	{	
		cout<<"Out of range"<<endl;
		exit(1);
	}
	
	return sPtr[subscript];//restituisce una copia

}
String String::operator()(int index,int subLength) const //restituisce  una sottostringa alla posizione index e di lunghezza subLength
{
	if (index<0 || index>=length || subLength<0) return " " ;//automaticamente convertito in oggetto String
	
	int len;//lunghezza sottostringa
	if ((subLength==0)|| (index + subLength>length))
		len = length-index;
		else
			len = subLength;
	
	char *tempPtr = new char[ len+1];
	strncpy(tempPtr,&sPtr[index],len);
	tempPtr[len]='\0';//aggiuge carattere di terminazione
	
	//crea oggetto String temporaneo che contiene la sottostringa
	String tempString(tempPtr);
	delete [] tempPtr; 
	return tempString;
}
int String::getLength()const
{

	return length;
}
//funzione utilità chiamata nei costruttori e in operator=
void String::setString(const char*string2)
{

	sPtr = new char[length +1];
	
	if (string2 !=0)//non èun puntatore nullo,posso copiare il contenuto
	
		
		strcpy(sPtr,string2);
	else
		//crea stringa vuota
		sPtr[0]='\0';//stringa vuota	

}
ostream& operator<<(ostream&output,const String&s)
{
	output<<s.sPtr;
	return output;//abilita operazioni in cascata
}
istream& operator>>(istream&input,String & s)
{
	char temp[100];//area buffer 
	input>>setw(100)>>temp;
	s = temp; //chiama la ridefinizione di = 
	
	return input;
}
int main(){
cout<<"Ridefinizione degli operatori \n";

	String s1("happy");
	String s2("birthday");
	String s3;
	
	//verifica degli operati ridefiniti relazionali e di uguaglianza
	cout<<boolalpha<<"s1 is " <<s1<<" s2 is " <<s2<<" s3 is " <<s3 ;
	cout<<"\n s1==s2 : "<< (s1==s2)
	<<"\n s1!=s2 : "<<(s1!=s2)
	<<"\n s2>s1 : "<< (s2>s1)
	<<"\n s2<s1 : "<<(s2<s1)
	<<"\n s2>=s1 : "<< (s2>=s1) 
	<<"\n s2<=s1 : "<< (s2<=s1 );
	
	//verifica dell'operatore di stringa vuota ! 
	cout<<"\n Testing |s3:"<<endl;
	
	if ( !s3)
	{
	
		cout<<"s3 is empty" <<endl;
		s3 = s1; //verifica assegnazione
		cout<<"s3 is : "<<s3 << " \n";
	}
	//verifica operatore ridefinito di concatenamento
	cout<<"s1+=s2 " <<endl;
	s1+=s2;
	cout<<s1;
	//verifica operatore di chiamata di funzione
	cout<<" La sottostringa di s1 che inizia da 0 per 14 caratteri è : "<< s1(0,14) <<"\n";
	//verifica "fino alla fine della stringa"
	cout<<s1(15)<<"\n";
	
	//verifica costruttore di copia
	String *s4 = new String(s1);
	cout<<"s4 is "<<*s4;
	
	//verifica autoassegnamento
	*s4 = *s4;
	
	s1[0]='%';
	s1[6]='$';
	cout<<"\ns1  ora è : "<<s1<<" \n ";
	//veriifca indice
	s1[30] = 'd';//Errrore
return 0;
}