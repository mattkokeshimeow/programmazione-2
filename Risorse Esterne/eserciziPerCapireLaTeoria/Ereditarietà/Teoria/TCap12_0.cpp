/*TCap12_Ereditarietà*/
/*Note:Vedere pagina 622 paragrafo 12.4. Questo esempio permette di evidenziare la relazione tra due classi senza sfruttare l'ereditarietà.
Il TCap12_1.cpp verrà usato lo stesso però con l'ereditarietà(che è più semplice). Nel terzo esempio si mostreranno gli errori tipici dell'ere
ditarietà. Nel quarto l'uso di protected nella classe base. Nel quinto l'uso di private*/

/*NOTE PROGRAMMA : Si lavora su un programma di gestione paghe di un'azineda. Gli agenti retribuiti per provviggione(classi base) ricevono
una percentuale sul venduto mentre gli agenti con fisso mensile e provviggione(classi derivate) ricevono fussso mensile più percentuale
sulle vendite.*/
/*NOTA TEORIA: l'ereditarietà è una relazione "è un " ad esempio , un quadrato è una figura , ma non vale il viceversa.Non tutte le figure 
sono dei quadrati. Inoltre ,l'ereditarietà si  usa quando due classi condividono del codice e cioè permette di scrivere due classi con lo 
stesso codice*/

#include<iostream>
using namespace std;



#include<string>
#include<iomanip>
class CommissionEmployee
{
	public:
		CommissionEmployee(const string&,const string &,const string &,double =0.0,double=0.0);
		
		void setFirstName(const string &);
		string getFirstName()const;
		
		void setLastName(const string &);
		string getLastName()const;
		
		void setSocialSecurityNumber(const string &);
		string getSocialSecurityNumber()const;
		
		void setGrossSales(double);//imposta il totale vendite
		double getGrossSales()const;
		
		
		void setCommissionRate(double);//imposta percentuale
		double getCommissionRate()const;
		
		double earnings()const;
		void print() const;
		
	private:
		string firstName;
		string lastName;
		string socialSecurityNumber;
		double grossSales;//totale vendite
		double commissionRate;//percentuale sulle vendite
		
};

CommissionEmployee::CommissionEmployee(const string &first,const string &last,const string & ssn,double sales,double rate)
{
	firstName = first;
	lastName =last;
	socialSecurityNumber = ssn;
	setGrossSales(sales);
	setCommissionRate(rate);
	

}
void CommissionEmployee::setFirstName(const string &first)
{
	firstName = first;
}
string CommissionEmployee::getFirstName()const 
{
	return firstName;
}
void CommissionEmployee::setLastName(const string &last)
{
	lastName = last;
}
string CommissionEmployee::getLastName() const
{
	return lastName;
}
void CommissionEmployee::setSocialSecurityNumber(const string &ssn)
{
	socialSecurityNumber = ssn;

}
string CommissionEmployee::getSocialSecurityNumber()const
{
	return socialSecurityNumber;
}
void CommissionEmployee::setGrossSales(double sales)
{
	grossSales = (sales<0.0)?0.0:sales;
} 
double CommissionEmployee::getGrossSales()const 
{
	return grossSales;
}
void CommissionEmployee::setCommissionRate(double rate)
{
	commissionRate = (rate>0.0 &&rate<1.0)? rate : 0.0;
}

double CommissionEmployee::getCommissionRate()const
{
	return commissionRate;
}
double CommissionEmployee::earnings()const
{
	return commissionRate*grossSales;
}
void CommissionEmployee::print() const
{
	cout<<"Commission Employee : "<<firstName<<' ' <<lastName
	<< "\nsocial security number:: " <<socialSecurityNumber
	<<"\ngross sales: "<<grossSales
	<<"\ncommission rate: "<<commissionRate;

}
//creazione della classe BasePluscommissionEmployee che praticamente è simile alla precedente ,nel file TCap12_1.cpp verrà usata l'ereditarietà

class BasePlusCommissionEmployee
{
	public:
		BasePlusCommissionEmployee(const string&,const string &,const string &,double =0.0,double=0.0,double =0.0);
		
		void setFirstName(const string &);
		string getFirstName()const;
		
		void setLastName(const string &);
		string getLastName()const;
		
		void setSocialSecurityNumber(const string &);
		string getSocialSecurityNumber()const;
		
		void setGrossSales(double);//imposta il totale vendite
		double getGrossSales()const;
		
		
		void setCommissionRate(double);//imposta percentuale
		double getCommissionRate()const;
		
		void setBaseSalary(double);//imposta il fisso mensile
		double getBaseSalary()const;
		
		double earnings()const;
		void print() const;
		
	private:
		string firstName;
		string lastName;
		string socialSecurityNumber;
		double grossSales;//totale vendite
		double commissionRate;//percentuale sulle vendite
		double baseSalary;//fisso mensile


};
BasePlusCommissionEmployee::BasePlusCommissionEmployee(const string &first,const string &last,const string & ssn,double sales,double rate,double bs)
{
	firstName = first;
	lastName =last;
	socialSecurityNumber = ssn;
	setGrossSales(sales);
	setCommissionRate(rate);
	setBaseSalary(bs);

}
void BasePlusCommissionEmployee::setFirstName(const string &first)
{
	firstName = first;
}
string BasePlusCommissionEmployee::getFirstName()const 
{
	return firstName;
}
void BasePlusCommissionEmployee::setLastName(const string &last)
{
	lastName = last;
}
string BasePlusCommissionEmployee::getLastName() const
{
	return lastName;
}
void BasePlusCommissionEmployee::setSocialSecurityNumber(const string &ssn)
{
	socialSecurityNumber = ssn;

}
string BasePlusCommissionEmployee::getSocialSecurityNumber()const
{
	return socialSecurityNumber;
}
void BasePlusCommissionEmployee::setGrossSales(double sales)
{
	grossSales = (sales<0.0)?0.0:sales;
} 
double BasePlusCommissionEmployee::getGrossSales()const 
{
	return grossSales;
}
void BasePlusCommissionEmployee::setCommissionRate(double rate)
{
	commissionRate = (rate>0.0 &&rate<1.0)? rate : 0.0;
}

double BasePlusCommissionEmployee::getCommissionRate()const
{
	return commissionRate;
}
void BasePlusCommissionEmployee::setBaseSalary(double salary)
{
	baseSalary = (salary<0.0 )? 0.0 : salary;
}

double BasePlusCommissionEmployee::getBaseSalary()const
{
	return baseSalary;
}
double BasePlusCommissionEmployee::earnings()const//restitusce retribuzione ovvero salario + proviggione
{
	return baseSalary + ( commissionRate*grossSales);
}
void BasePlusCommissionEmployee::print() const
{
	cout<<"Commission Employee : "<<firstName<<' ' <<lastName
	<< "\nsocial security number:: " <<socialSecurityNumber
	<<"\ngross sales: "<<grossSales
	<<"\ncommission rate: "<<commissionRate
	<<"\nbase salary : " <<baseSalary ;

}
int main(){
cout<<"Ereditarietà "<<endl;
	CommissionEmployee employee ( "Ale","DiStefano","111",10000,.06);
	cout <<fixed<<setprecision(2); //imposta la formattazione dei valori in double ( precisa che ci saranno due cifre dopo la virgola)
	cout<<"Employee information obtainted by get functions: \n"
	<<" \nFirst name is " <<employee.getFirstName()
	<<"\nLast name is "<<employee.getLastName()
	<<"\nSocial security number is "
	<<employee.getSocialSecurityNumber()
	<<"\nGross sales is "<<employee.getGrossSales()
	<<"\nCommission rate is "<< employee.getCommissionRate() <<endl;

	employee.setGrossSales(8000);//imposta totale vendite
	employee.setCommissionRate(.1);//imposta percentuale
	employee.print();
	cout<<"\n Employee's earnings: $" <<employee.earnings() <<endl;//visualizza retribuzione

//agenti che percepiscono anche un salario fisso mensile
	BasePlusCommissionEmployee employeeTwo("Daniela","Ricca","222",10000,.06,200);
	employeeTwo.setBaseSalary(1500);//imposto salario fisso mensile
	employeeTwo.print();
	cout<<"\n Employee's earnings: $" <<employeeTwo.earnings() <<endl;//visualizza retribuzione


return 0;
};