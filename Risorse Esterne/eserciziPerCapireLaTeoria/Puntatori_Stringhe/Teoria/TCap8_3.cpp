/*Uso di puntatori a funzione e di array di puntatori a funzione*/
#include<iostream>
using namespace std;
void SelectionSort( int v[],const int size, bool (*) ( int ,int) ) ; //prototipo del puntatore a funzione
void Swipe(int &,int & ) ;
bool Ascending( int ,int);
bool Descending(int ,int);

bool Descending(int smallest,int actually ) {

	if ( smallest<actually)
		return true;
		
return false;

}
bool Ascending(int smallest,int actually){

	if ( smallest>actually)
		return true;
		
return false;


}

void SelectionSort(int v [],const int size ,bool (*typeOrder) (int smallest,int actually )) {

int min;

for (int i =0; i<size-1;i++)
{
	min = i ;
	for(int j = i+1;j<size;j++)
		if( (*typeOrder)(v[min],v[j])) // bisogna deferenziare ils punatore a funzione e quindi si usa l'asterisco nel puntatore "typeOrder".
			min = j ;

	Swipe(v[min],v[i]);

}


}
void Swipe(int &smallest,int & actually){

int alias = actually;
actually = smallest;
smallest = alias;


}

//------metodi per array di puntatori a funzione che si usano nei menu a scelta-----
void functionOne(int );
void functionTwo(int );
void functionThree(int);

void functionOne(int num){

cout<<"Scelto numero " <<num <<"funzioneONe" <<endl;

}
void functionTwo(int num){

cout<<"Scelto numero " <<num <<"funzioneTwo" <<endl;

}
void functionThree(int num){

cout<<"Scelto numero " <<num <<"funzioneThree" <<endl;

}


int main(){
cout<<"Puntatori e stringhe3"<<endl;

int v[] = {4,3,2,1,0};
int n = sizeof(v)/sizeof(int);
int order =0;

cout<<"Scegli ordinamento : crescente tasto 0 ; \ndecrescente tasto 1 " ;
int key =0;
cin >>key;

if (key==0) 
	SelectionSort(v,n,Ascending);
else 
	SelectionSort(v,n,Descending);
//stampa
for (int i =0;i<n ; i++)
	cout<< v[i] << " " ;
	
//---------------------------------------parte sull'array di puntatori a funzione----

//inizializzare un array di funzione.Dovrà avere la forma di funzione

void (*array[3]) (int ) = { functionOne,functionTwo,functionThree};


int choice;

cout<<"Scegli numero tra 0 e 2 "<<endl;
cin>>choice;

(*array[choice])(choice); // IN questo caso array[choice] chiama il puntatore alla funzione di choiche. Tale puntatore è un pntatore a una funzione che deve essere deferenziato con*
	
return 0;

}

/*NOTE:
1) scrivere : bool (*nomeFunzione) ( int par1,int par2 )  è diverso da bool *nomeFunzione (int par1,int par2) . Il primo
è un puntatore ad una funzione ,mentre il secondo è una funzione che restituisce un puntatore ad un valore bool. */