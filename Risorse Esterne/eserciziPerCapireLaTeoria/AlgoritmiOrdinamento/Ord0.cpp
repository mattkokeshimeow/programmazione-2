/*TCap8_8.cpp. Algoritmi d'ordinamento*/
#include<iostream>
using namespace std;


void stampa(int v[],int n)
{

	cout<<"\nInizio stampa\n";

	for (int i=0;i<n;i++)
		cout<<v[i]<<" ";
		
		cout<<"\nFine stampa\n";
}
void stampaQ( int left, int right ,int v[])
{

	if (left>right)return ;
		static int n =0;
n++;
	cout<<"--Num: "<<n<<endl;
	cout<<"------"<<endl;
	for(int i =left;i<=right;i++)
		cout<<v[i]<<" ";
	cout<<"\n-------"<<endl<<endl;
}
void swipe( int i   ,int j ,int v[])
{

	int temp = v[i];
	v[i] = v[ j ];
	v[ j] = temp;

}
void QuickSort( int v[],int left,int right)
{
	if (left >= right) {
        return;
    }
	int i = left; int j = right-1; int mid =  i + (j-i)/2;
	int pivot = v[ mid];
	swipe ( mid , right,v); //scambio il pivot con l'ultimo elemento
	

	while(i<=j)
	{
		while( i<=j && v[i]<=pivot) i++;
		while(i<=j &&   v[j]>pivot) j--;
		
		if( i<j) //non si scambiano perchè i due elementi stanno già nela parte corretta
		{
			swipe( i , j ,v);
			i++;
			j--;
		}
	}
	swipe( i , right,v); //dove i è la posizione del pivot . Lo scambio col pivot iniziale che si trova alla
	// fine(right)Difatti j partiva da right-1(per lasciare il pivot al suo posto)
	
	stampa(left,j,v);  //partizione sinistra
	stampa(i+1,right,v);//partizione destra

	QuickSort(v ,left,j);
	QuickSort(v,i+1,right);

}

void reset(int v[],int n)
{
	int s =0;
	for (int i =n-1;i>=0;i--)
		{
			v[s] = i;
			s++;
		}
}
void Merge(int left,int center,int right,int v[],int dim )
{
	int i =left;
	int middle = center;
	int j = center+1;
	int k = left;
	const int n = dim;
	int b[n ];
	
	
	
//confronta gli elementi dei due sotto array( left -middle) e (middle+1 ,right) e metti nell'array di supporto l'elemento più piccolo
	while ( i<=center && j<=right)
	{
		if (v[i]<v[j] )
		{
			b[k] = v[i];
			k++;
			i++;
		
		}else
			{
				b[k] = v[j];
				k++;
				j++;
			
			}
	}
	
	//a questo punto uno dei due sottoarray sarà ordinato bisogna rimettere gli elementi di quello non ordinato nell'array di appoggio
	while(i<=center) //rielaborare elementi array di sinistra
	{
		b[k] = v[i];
		i++;
		k++;
	
	}
	//oppure rielaborare array di destra(solo uno dei due while verrà eseguito
	while(j<=right)
	{
		b[k] = v[j];
		j++;
		k++;
	
	}
	//copiare array da quello di supporto in quello in origine
	for( k = left; k<=right;k++)
		v[k] = b[k];


}
void  MergeSort(int left,int right,int v[],int n)
{
	if (left<right)
	{
		//dividi l'array in sottoarray fin quando non si arriva a sottoarray di dimensione = 1
		
		int middle = (left+right)/2;
		stampaQ(left,middle,v);
		MergeSort(left,middle,v,n);
		stampaQ(middle+1,right,v);
		MergeSort(middle+1,right,v,n);
		Merge(left,middle,right,v,n);
		
	}



}
int BinarySearch(int key,int left,int right,int v[])
{
	int i = left;
	int j = right;
	int r =-1;
	
	do 
	{
		int middle = ( i+j)/2;

		if (v[middle] ==key )
			r= 0;
		else
			if (v[middle]>key)
				j = middle-1;
			else
				i = middle+1;
			
			
	
	
	
	}while ( r ==-1 && i<=j);

	return r;

}
void InsertionSort(int v[],int n)
{
	int index;
	int temp;
	
	for(int i =1; i< n ;i++)
	{
		temp = v[i]; //elemento attuale da ordinare
		index = i-1; //indice precedente 
	
		//il ciclo continua fin quando non trovo un elemento più piccolo di temp.
		//in tale ciclo gli elementi si spostano di un posto a destra per fare spazio a temp
		while ( index>=0 && v[index]>temp)
		{
			v[index+1] = v[index]; //lascio uno spazio a sinistra
			index--;
	
		}
		v[index+1] = temp;
	
	}


}
void insertionSort( int v[],int n){

	for(int i=1 ; i< n ; i++){
	
	int j = i-1;
	int toInsert = v[ i ];
	while ( j>=0 && toInsert < v[ j ])
	{
		v[ j+1 ] = v[ j ];

		j--;
	}
	v[ j +1 ] = toInsert;
	}
}
void BubbleSort(int v [],int n){

	bool ord = false;
for(int i = 0; i< n -1 && ord==false; i++){
	for(int j = 0 ; j< n-1-i ; j++)
	{
		ord = true;
		if ( v[ j+1 ] < v[ j ] )
		{	
			int temp = v[ j ];
			v[ j ] = v[ j +1 ];
			v[ j +1 ] = temp;
			ord = false;
		}
	
	}


}
}

int main()
{
	
	int n =5;
	int v[n];
	reset (v,n);
	cout<<"\n---QuickSort---\n";
	stampaQ(0,n-1,v);
	QuickSort(0,n-1,v);
	cout<<"\n---PostQuickSort---\n";
	stampa(v,n);
	reset(v,n);
	cout<<"\n---MergeSort---\n";
		stampa(v,n);
	MergeSort(0,n-1,v,n);
		cout<<"\n---PostMergeSort---\n";

	cout<<"\n---InserctionSort---\n";

	stampa(v,n);
	reset(v,n);
	InserctionSort(v,n);
	cout<<"\n---PostInserctionSort---\n";


int r =  BinarySearch(-1,0,n-1, v);

if (r ==-1) cout<<"Non trovato "<<endl;
else 
	cout<<"Trovato"<<endl;


};